package com.dt.notes.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.dt.notes.application.Application;
import com.dt.notes.config.Configuration;
import com.dt.notes.dao.AlarmDAO;
import com.dt.notes.dao.LabelDAO;
import com.dt.notes.dao.NoteDAO;
import com.dt.notes.dao.NoteLabelsDAO;
import com.dt.notes.entities.Alarm;
import com.dt.notes.entities.Label;
import com.dt.notes.entities.Note;
import com.dt.notes.entities.NoteLabelsCrossRef;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Database(entities = {Note.class, Alarm.class, Label.class, NoteLabelsCrossRef.class}, version = 1,  exportSchema = false)
public abstract class NoteDatabase extends RoomDatabase {
    private static String DATABASE_NAME = "Notes_Database";
    private static NoteDatabase instance;

    public abstract NoteDAO noteDAO();
    public abstract AlarmDAO alarmDAO();
    public abstract LabelDAO labelDAO();
    public abstract NoteLabelsDAO noteLabelsDAO();

    public static synchronized NoteDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), NoteDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }

        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            new PopulateDatabaseAsyncTask(instance).execute();
        }
    };

    private static class PopulateDatabaseAsyncTask extends AsyncTask<Void, Void, Void> {
        private NoteDAO noteDAO;
        private LabelDAO labelDAO;
        private NoteLabelsDAO noteLabelsDAO;

        private PopulateDatabaseAsyncTask(NoteDatabase noteDatabase) {
            this.noteDAO = noteDatabase.noteDAO();
            this.labelDAO = noteDatabase.labelDAO();
            this.noteLabelsDAO = noteDatabase.noteLabelsDAO();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            List<String> colors = new ArrayList<>();
            List<String> labels = new ArrayList<>();
            Random r = new Random();

            try {
                colors = Configuration.getColors(Application.getAppContext());
                labels = Configuration.getDefaultLabels(Application.getAppContext());
            } catch (Exception ignored) {

            }

            long noteId = 0;
            long labelId = 0;

            for(int i = 0; i < 100; i++) {
                noteId= noteDAO.insert(new Note("Title " + i, "Description " + i, colors.get(r.nextInt(colors.size() - 1)), false, Math.random() < 0.5));
            }

            for(int i = 0; i < labels.size(); i++) {
                labelId = labelDAO.insert(new Label(labels.get(i)));
            }

            noteLabelsDAO.insert(new NoteLabelsCrossRef(noteId, labelId));

            return null;
        }
    }
}
