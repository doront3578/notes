package com.dt.notes.util;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.dt.notes.R;

/**
 * <h1>NotificationHelper</h1>
 *
 * <p>
 * A helper class to handle notifications for various API versions.
 * Allows channel creation and ignores them for unsupported API versions.
 * </p>
 */
public class NotificationHelper extends ContextWrapper {
    // Constants
    public static final String REMINDERS_CHANNEL_ID = "REMINDERS_CHANNEL";
    public static final int REMINDERS_CHANNEL_NAME = R.string.reminders_channel_name;
    public static final int REMINDERS_CHANNEL_DESCRIPTION = R.string.reminders_channel_description;

    // Properties
    private NotificationManager mNotificationManager;

    public NotificationHelper(Context base) {
        super(base);

        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            createChannels();
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannels() {
        NotificationChannel remindersChannel = new NotificationChannel(REMINDERS_CHANNEL_ID, getApplicationContext().getResources().getString(REMINDERS_CHANNEL_NAME), NotificationManager.IMPORTANCE_DEFAULT);
        remindersChannel.setDescription(getApplicationContext().getResources().getString(REMINDERS_CHANNEL_DESCRIPTION));
        remindersChannel.enableLights(true);
        remindersChannel.enableVibration(true);
        remindersChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        getNotificationManager().createNotificationChannel(remindersChannel);
    }

    public NotificationManager getNotificationManager() {
        if(mNotificationManager == null)
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        return mNotificationManager;
    }

    public NotificationCompat.Builder getRemindersChannelNotification(String title, String massage) {
        return new NotificationCompat.Builder(getApplicationContext(), REMINDERS_CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(massage)
                .setSmallIcon(R.drawable.ic_notification);
    }
}
