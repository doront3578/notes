package com.dt.notes.util;

public class CustomSpinnerItem {
    private String mItemDispName;
    private String mSecondaryData;
    public String mData;

    public CustomSpinnerItem(String displayName, String secondaryData, String all) {
        mItemDispName = displayName;
        mSecondaryData = secondaryData;
        mData = all;
    }

    public String getmItemDispName() {
        return mItemDispName;
    }

    public String getmSecondaryData() {
        return mSecondaryData;
    }

    public void setmItemDispName(String dispName) {
        mItemDispName = dispName;
    }

    public void setmSecondaryData(String secondaryData) {
        mSecondaryData = secondaryData;
    }
}
