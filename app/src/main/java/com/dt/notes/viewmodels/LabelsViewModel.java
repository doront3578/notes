package com.dt.notes.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.dt.notes.entities.Label;
import com.dt.notes.repositories.LabelsRepository;

import java.util.List;

public class LabelsViewModel extends AndroidViewModel {
    private LabelsRepository labelsRepository;

    public LabelsViewModel(@NonNull Application application) {
        super(application);

        labelsRepository = new LabelsRepository(application);
    }

    public LiveData<List<Label>> getAllLabels() {
        return labelsRepository.getAllLabels();
    }

    public List<Label> getAllLabelsSync() {
        return labelsRepository.getAllLabelsSync();
    }

    public void insert(Label label) {
        labelsRepository.insert(label);
    }

    public void update(Label label) {
        labelsRepository.update(label);
    }

    public void delete(Label label) {
        labelsRepository.delete(label);
    }

    public void deleteById(Integer id) {
        labelsRepository.deleteById(5);
    }

    public Label getLabelByName(String name) {
      for (Label l : getAllLabelsSync()) {
          if (l != null && l.getName().equals(name))
              return l;
      }

      return null;
    }
}
