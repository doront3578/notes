package com.dt.notes.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dt.notes.common.ActivityToolbarMode;
import com.dt.notes.common.NotesListChannel;
import com.dt.notes.helpers.ToolbarsManager;

/**
 * The view model for the {@link com.dt.notes.activities.NotesListActivity} Activity.
 */
public class NotesListActivityViewModel extends ViewModel {
    // Properties
    /**
     * The current channel of notes fed to the main recycler view
     */
    private MutableLiveData<NotesListChannel> mCurrentNotesListChannel;

    /**
     * The current mode the activity is in right now
     */
    private ActivityToolbarMode mMode;

    private ToolbarsManager mToolbarsManager;

    private int mRecyclerViewLayoutSpan;

    public NotesListActivityViewModel() {
        mCurrentNotesListChannel = new MutableLiveData<>();
        mToolbarsManager = new ToolbarsManager();

        mCurrentNotesListChannel.setValue(NotesListChannel.UNSET);
        mRecyclerViewLayoutSpan = 1;
        mMode = ActivityToolbarMode.NORMAL;
    }

    public LiveData<NotesListChannel> getCurrentChannel() {
        return mCurrentNotesListChannel;
    }

    public void setCurrentChannel(NotesListChannel notesListChannel) {
        mCurrentNotesListChannel.setValue(notesListChannel);
    }

    public ActivityToolbarMode getMode() {
        return mMode;
    }

    public void setMode(ActivityToolbarMode mode) {
        mMode = mode;
    }

    public ToolbarsManager getToolbarsManager() {
        return mToolbarsManager;
    }

    public int getRecyclerViewLayoutSpan() {
        return mRecyclerViewLayoutSpan;
    }

    public void setRecyclerViewLayout(int mRecyclerViewLayout) {
        this.mRecyclerViewLayoutSpan = mRecyclerViewLayout;
    }
}