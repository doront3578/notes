package com.dt.notes.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.dt.notes.common.NotesListChannel;
import com.dt.notes.entities.Note;
import com.dt.notes.entities.NoteLabels;
import com.dt.notes.repositories.NotesRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>NoteViewModel</h1>
 *
 * <p>
 * The view model for the note entity. abstracts the access to the repository data by exporting
 * CRUD methods for it.
 * </p>
 */
public class NoteViewModel extends AndroidViewModel {
    // Properties
    private NotesRepository repository;

    public NoteViewModel(@NonNull Application application) {
        super(application);

        repository = new NotesRepository(application);
    }

    public long insert(Note note) {
        return repository.insert(note);
    }

    public void update(Note note) {
        repository.update(note);
    }

    public void delete(Note note) {
        repository.delete(note);
    }

    public void moveAllNotesToTrash() {
        repository.moveAllNotesToTrash();
    }

    public LiveData<List<Note>> getAllExisting() {
        return repository.getAllExisting();
    }

    public LiveData<List<Note>> getAllDeleted() {
        return repository.getAllDeleted();
    }

    public void moveToTrash(Note note) {
        repository.moveToTrash(note);
    }

    public void restoreFromTrash(Note note) {
        repository.restoreFromTrash(note);
    }

    public LiveData<List<Note>> getAllPrivate() {
        return repository.getAllPrivate();
    }

    public Note get(int id) {
        return repository.get(id);
    }

    public LiveData<NoteLabels> getNoteLabels(int noteId) {
        return repository.getNoteLabels(noteId);
    }

    public NoteLabels getNoteLabelsSync(int noteId) {
        return repository.getNoteLabelsSync(noteId);
    }

    public void addLabelToNote(int noteId, int labelId) {
        repository.addLabelToNote(noteId, labelId);
    }

    public void removeLabelFromNote(int noteId, int labelId) {
        repository.removeLabelFromNote(noteId, labelId);
    }

    public void deleteLabelsOfNote(int noteId) {
        repository.deleteLabelsOfNote(noteId);
    }

    public List<Note> getNotesFromChannel(NotesListChannel channel) {
        switch (channel) {
            case NORMAL:
                return getAllExisting().getValue();
            case DELETED:
                return getAllDeleted().getValue();
            case PRIVATE:
                return getAllPrivate().getValue();
        }

        return new ArrayList();
    }
}
