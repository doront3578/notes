package com.dt.notes.viewmodels;

import android.util.SparseBooleanArray;

import androidx.lifecycle.ViewModel;

/**
 * <h1>SelectedItemsViewModel</h1>
 *
 * <p>
 * A {@link ViewModel} to hold selected items for components who implement a selection mode.
 * </p>
 */
public class SelectedItemsViewModel extends ViewModel{
    /**
     * The list of selected items
     */
    public SparseBooleanArray selectedItems = new SparseBooleanArray();
}
