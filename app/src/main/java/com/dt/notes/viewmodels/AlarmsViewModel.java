package com.dt.notes.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.dt.notes.entities.Alarm;
import com.dt.notes.repositories.AlarmsRepository;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * <h1>AlarmsViewModel</h1>
 *
 * <p>
 * The view model for the alarm entity. abstracts the access to the repository data by exporting
 * CRUD methods for it.
 * </p>
 */
public class AlarmsViewModel extends AndroidViewModel {
    private AlarmsRepository repository;

    public AlarmsViewModel(@NonNull Application application) {
        super(application);

        repository = new AlarmsRepository(application);
    }

    public long insert(Alarm alarm) {
        try {
            return repository.insert(alarm);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public void update(Alarm alarm) {
        repository.update(alarm);
    }

    public void delete(Alarm alarm) {
        repository.delete(alarm);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public LiveData<List<Alarm>> getAll() {
        return repository.getAll();
    }

    public Alarm get(int id) {
        return repository.get(id);
    }

    public Alarm getByNote(int noteId) {
        return repository.getByNote(noteId);
    }
}
