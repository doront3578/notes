package com.dt.notes.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.dt.notes.R;
import com.dt.notes.adapters.CustomSpinnerAdapter;
import com.dt.notes.util.CustomSpinnerItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class DateTimePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private Calendar chosenDateTime;
    private OnDateTimePickListener onDateTimePickListener;
    private List<CustomSpinnerItem> mDateItems;
    private List<CustomSpinnerItem> mTimeItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.date_time_dialog_layout, container, false);
        Spinner dateSpinner = view.findViewById(R.id.date_picker);
        Spinner timeSpinner = view.findViewById(R.id.time_picker);
        Button cancelButton = view.findViewById(R.id.cancelPicker);
        Button saveReminderButton = view.findViewById(R.id.saveReminderButton);

        populateSpinnerItems();

        chosenDateTime = Calendar.getInstance();

        ArrayAdapter<CustomSpinnerItem> dateAdapter = new CustomSpinnerAdapter(getContext(), mDateItems);
        dateSpinner.setAdapter(dateAdapter);

        final ArrayAdapter<CustomSpinnerItem> timeAdapter = new CustomSpinnerAdapter(getContext(), mTimeItems) {
            @Override
            public boolean isEnabled(int position) {
                return super.isEnabled(position);
            }
        };
        timeSpinner.setAdapter(timeAdapter);

        dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String[] s = getResources().getStringArray(R.array.date_picker_spinner);
                CustomSpinnerItem item = (CustomSpinnerItem) parentView.getItemAtPosition(position);

                for (String option : s) {
                    String[] split = option.split("\\|", 2);

                    // Must be on of the "|" options
                    if (item.getmItemDispName().equals(split[0])) {
                        if (split.length == 2) {
                            chosenDateTime.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                            chosenDateTime.add(Calendar.DATE, Integer.valueOf(split[1]));

                            break;
                        }
                        // Pick a date
                        else {
                            Calendar c = Calendar.getInstance();
                            int year = c.get(Calendar.YEAR);
                            int month = c.get(Calendar.MONTH);
                            int day = c.get(Calendar.DAY_OF_MONTH);
                            DatePickerDialog dp = new DatePickerDialog(getContext(), DateTimePickerDialogFragment.this, year, month, day);
                            dp.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                            dp.show();

                            break;
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, final View selectedItemView, int position, long id) {
                String[] s = getResources().getStringArray(R.array.time_picker_spinner);

                CustomSpinnerItem item = (CustomSpinnerItem) parentView.getItemAtPosition(position);

                for (String option : s) {
                    String[] split = option.split("\\|", 2);
                    String[] mData = item.mData.split("\\|", 2);

                    // Must be on of the "|" options
                    if (mData[0].equals(split[0])) {
                        if (split.length == 2) {
                            java.text.DateFormat dateFormat = new SimpleDateFormat("hh:mm", Locale.getDefault());
                            try {
                                Calendar c = Calendar.getInstance();
                                c.setTime(Objects.requireNonNull(dateFormat.parse(mData[1])));

                                chosenDateTime.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY));
                                chosenDateTime.set(Calendar.MINUTE, c.get(Calendar.MINUTE));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;
                        }
                        // Pick a time
                        else {
                            Calendar calendar = Calendar.getInstance();
                            int hours = calendar.get(Calendar.HOUR_OF_DAY);
                            int minutes = calendar.get(Calendar.MINUTE);
                            TimePickerDialog tpd = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                    Calendar datetime = Calendar.getInstance();
                                    Calendar c = Calendar.getInstance();
                                    datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                    datetime.set(Calendar.MINUTE, minute);
                                    if (datetime.getTimeInMillis() >= c.getTimeInMillis()) {
                                        chosenDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                        chosenDateTime.set(Calendar.MINUTE, minute);
                                        chosenDateTime.set(Calendar.SECOND, 0);
                                    } else {
                                        Toast.makeText(getContext(), "Invalid Time", Toast.LENGTH_LONG).show();
                                        String text = (String) ((TextView) ((ConstraintLayout) selectedItemView).getViewById(R.id.primaryText)).getText();

                                        for (CustomSpinnerItem item : mTimeItems) {
                                            if (item.getmItemDispName().equals(text)) {
                                                item.setmSecondaryData("Invalid time");
                                            }
                                        }

                                        timeAdapter.notifyDataSetChanged();
                                    }
                                }
                            }, hours, minutes, DateFormat.is24HourFormat(getActivity()));
                            tpd.show();

                            break;
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                Toast.makeText(DateTimePickerDialogFragment.this.getContext(), ":)", Toast.LENGTH_SHORT).show();
            }

        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        saveReminderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onDateTimePickListener != null)
                    onDateTimePickListener.onDateTimePickFinished(chosenDateTime);
            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            onDateTimePickListener = (OnDateTimePickListener) getActivity();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        chosenDateTime.set(Calendar.YEAR, year);
        chosenDateTime.set(Calendar.MONTH, month);
        chosenDateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar datetime = Calendar.getInstance();
        Calendar c = Calendar.getInstance();
        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        datetime.set(Calendar.MINUTE, minute);
        if (datetime.getTimeInMillis() >= c.getTimeInMillis()) {
            chosenDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            chosenDateTime.set(Calendar.MINUTE, minute);
            chosenDateTime.set(Calendar.SECOND, 0);
        } else {
            Toast.makeText(getContext(), "Invalid Time", Toast.LENGTH_LONG).show();
        }
    }

    public interface OnDateTimePickListener {
        void onDateTimePickFinished(Calendar calendar);

        void onDateTimePickerDismiss();
    }

    private void populateSpinnerItems() {
        String[] dateItems = getResources().getStringArray(R.array.date_picker_spinner);
        String[] timeItems = getResources().getStringArray(R.array.time_picker_spinner);
        mDateItems = new ArrayList<>();
        mTimeItems = new ArrayList<>();

        for (String string : dateItems) {
            String[] splitResult = string.split("\\|", 2);

            if (splitResult.length == 2) {
                Date dt = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(dt);
                c.add(Calendar.DATE, Integer.valueOf(splitResult[1]));
                dt = c.getTime();

                // Then get the day of week from the Date based on specific locale.
                String dayOfWeek = new SimpleDateFormat("EEEE", Locale.getDefault()).format(dt);

                mDateItems.add(new CustomSpinnerItem(splitResult[0], dayOfWeek, string));
            } else
                mDateItems.add(new CustomSpinnerItem(string, null, string));
        }

        for (String string : timeItems) {
            String[] splitResult = string.split("\\|", 2);

            if (splitResult.length == 2)
                mTimeItems.add(new CustomSpinnerItem(splitResult[0], splitResult[1], string));
            else
                mTimeItems.add(new CustomSpinnerItem(splitResult[0], null, string));
        }
    }
}
