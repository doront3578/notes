package com.dt.notes.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dt.notes.R;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import top.defaults.drawabletoolbox.DrawableBuilder;

/**
 * A color picker implemented as a bottom sheet dialog.
 */
public class ColorPickerFragment extends BottomSheetDialogFragment {
    // Constants
    private static final String COLORS = "com.dt.notes.fragments.COLORS";

    // Properties
    private ColorPickListener mColorPickListener;
    private List<String> mColors;

    public static ColorPickerFragment newInstance(List<String> colors) {
        Bundle args = new Bundle();
        args.putStringArrayList(COLORS, (ArrayList<String>) colors);

        ColorPickerFragment colorPickerFragment = new ColorPickerFragment();

        // Adds the colors as arguments to the fragment to maintain their existence throughout life cycles
        colorPickerFragment.setArguments(args);

        return colorPickerFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Gets the colors from the arguments bundle
        if (getArguments() != null && getArguments().getStringArrayList(COLORS) != null)
            mColors = getArguments().getStringArrayList(COLORS);
        else
            mColors = new ArrayList<>();

        return inflater.inflate(R.layout.bottom_sheet_layout, container, false);
    }

    /**
     * Creates the color buttons and adds them to the flex layout to be presented.
     */
    private void renderPickerColors() {
        int BUTTON_DP = 60;
        int DEFAULT_LOGICAL_DENSITY = 1;
        int DEFAULT_BUTTON_SIZE_PX = 60;

        float screenLogicalDensity = DEFAULT_LOGICAL_DENSITY;
        int buttonDimensions = DEFAULT_BUTTON_SIZE_PX;

        // Converts the desired button dp dimensions to pixels
        if (getActivity() != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

            screenLogicalDensity = metrics.density;
            buttonDimensions = (int) Math.ceil(BUTTON_DP * screenLogicalDensity);
        }


        if (getView() != null) {
            FlexboxLayout colorPickerLayout = getView().findViewById(R.id.flexLayout);

            // Iterating through the colors and creating an image button for each
            for (int color = 0; color < mColors.size(); color++) {
                final String colorCode = mColors.get(color);

                ImageButton imageButton = new ImageButton(getContext());
                imageButton.setLayoutParams(new FlexboxLayout.LayoutParams(buttonDimensions, buttonDimensions));
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) imageButton.getLayoutParams();
                marginLayoutParams.setMargins(0, (int) Math.ceil(5 * screenLogicalDensity), 0, 0);
                marginLayoutParams.setMarginStart((int) Math.ceil(5 * screenLogicalDensity));
                imageButton.setLayoutParams(marginLayoutParams);
                imageButton.setElevation(0);

                // Creating the drawable that sets the ripple, circular shape and color of the button
                final Drawable colorDrawable = new DrawableBuilder()
                        .oval()
                        .solidColor(Color.parseColor(mColors.get(color)))
                        .ripple()
                        .build();

                imageButton.setBackground(colorDrawable);

                // Adds the button to the color picker layout
                colorPickerLayout.addView(imageButton);

                // Triggering the on color pick click to the listener
                imageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mColorPickListener != null)
                            mColorPickListener.onColorPick(colorCode);
                    }
                });

            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Overriding the default behavior of the dialog creation
        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogObject) {
                BottomSheetDialog dialog = (BottomSheetDialog) dialogObject;
                FrameLayout bottomSheet = dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

                if (bottomSheet != null) {
                    BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                    // Dismiss the dialog when it is not visible (collapsed)
                    behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

                        @Override
                        public void onStateChanged(@NonNull View bottomSheet, int newState) {
                            if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                                dismiss();
                            }
                        }

                        @Override
                        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                        }
                    });

                    // Fixes the half expanded dialog issue on landscape
                    behavior.setPeekHeight(0);
                }
            }
        });

        return bottomSheetDialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        // Forcing the creator component to implement the interface.
        try {
            mColorPickListener = (ColorPickListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ColorPickListener");
        }
    }

    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        // Disabling the dim background effect of the dialog
        if (dialog.getWindow() != null)
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        renderPickerColors();
    }

    /**
     * <h1>ColorPickerListener</h1>
     *
     * <p>
     * An interface to be implemented by the component (activity, fragment) that created this
     * color picker. Allows the component to receive callbacks regarding color picks.
     * </p>
     */
    public interface ColorPickListener {
        /**
         * Invoked when a color button was clicked
         *
         * @param color The color hex code as string
         */
        void onColorPick(String color);
    }
}
