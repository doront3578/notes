package com.dt.notes.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.dt.notes.R;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class MultiChoicePickerFragment extends DialogFragment {
    // Constants
    private static final String LIST_OF_ITEMS = "MultiChoicePickerFragment.LIST_OF_ITEMS";
    private static final String CHECKED_ITEMS = "MultiChoicePickerFragment.CHECKED_ITEMS";
    private static final String BUTTONS_NAMES = "MultiChoicePickerFragment.BUTTONS_NAMES";

    // Members
    private DialogButtonsListener mListener;

    public static MultiChoicePickerFragment newInstance(String[] list, boolean[] checkedItems, @Nullable int[] buttonsNames) {
        Bundle args = new Bundle();
        args.putStringArray(LIST_OF_ITEMS, list);
        args.putBooleanArray(CHECKED_ITEMS, checkedItems);

        if (buttonsNames != null)
            args.putIntArray(BUTTONS_NAMES, buttonsNames);

        MultiChoicePickerFragment multiChoicePickerFragment = new MultiChoicePickerFragment();

        // Adds the lists as arguments to the fragment to maintain their existence throughout life cycles
        multiChoicePickerFragment.setArguments(args);

        return multiChoicePickerFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pick labels");

        builder.setMultiChoiceItems(getArguments().getStringArray(LIST_OF_ITEMS), getArguments().getBooleanArray(CHECKED_ITEMS), new DialogInterface.OnMultiChoiceClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b)
            {
                Toast.makeText(getActivity(), "item clicked at " + i, Toast.LENGTH_SHORT).show();
            }
        });

        int[] buttonsNames = getArguments().getIntArray(BUTTONS_NAMES);

        if (buttonsNames != null) {
            builder.setPositiveButton(buttonsNames[0], new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mListener != null)
                        mListener.onPositiveButtonClick();
                }
            });

            builder.setNegativeButton(buttonsNames[1], new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mListener != null)
                        mListener.onNegativeButtonClick();
                }
            });

            builder.setNeutralButton(buttonsNames[2], new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mListener != null)
                        mListener.onNeutralButtonClick();
                }
            });
        }

        Dialog dialog = builder.create();

        return dialog;
    }

    public void setDialogListener(DialogButtonsListener listener) {
        mListener = listener;
    }

    public interface DialogButtonsListener {
        void onPositiveButtonClick();
        void onNegativeButtonClick();
        void onNeutralButtonClick();
    }
}
