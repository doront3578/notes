package com.dt.notes.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.dt.notes.R;
import com.dt.notes.entities.Label;

import java.util.List;

public class LabelsAdapter extends RecyclerView.Adapter<LabelsAdapter.ViewHolder> {
    private List<Label> mData;

    // data is passed into the constructor
    public LabelsAdapter(List<Label> data) {
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.label_row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String labelName = mData.get(position).getName();
        holder.myTextView.setText(labelName);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.label_name);
        }

    }

    public void setData(List<Label> newData) {
        if (mData != null) {
            LabelDiffCallback postDiffCallback = new LabelDiffCallback(mData, newData);
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(postDiffCallback);

            mData.clear();
            mData.addAll(newData);
            diffResult.dispatchUpdatesTo(this);
        } else {
            // first initialization
            mData = newData;
        }
    }

    class LabelDiffCallback extends DiffUtil.Callback {

        private final List<Label> oldLabels, newLabels;

        public LabelDiffCallback(List<Label> oldLabels, List<Label> newLabels) {
            this.oldLabels = oldLabels;
            this.newLabels = newLabels;
        }

        @Override
        public int getOldListSize() {
            return oldLabels.size();
        }

        @Override
        public int getNewListSize() {
            return newLabels.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldLabels.get(oldItemPosition).getId() == newLabels.get(newItemPosition).getId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldLabels.get(oldItemPosition).equals(newLabels.get(newItemPosition));
        }
    }

    // convenience method for getting data at click position
    Label getItem(int id) {
        return mData.get(id);
    }
}
