package com.dt.notes.adapters;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dt.notes.R;
import com.dt.notes.application.Application;

import java.util.ArrayList;
import java.util.List;

public class CheckBoxIPickerAdapter<T> extends RecyclerView.Adapter {
    private List<T> mData;

    // data is passed into the constructor
    public CheckBoxIPickerAdapter(List<T> data) {
        this.mData = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkbox_picker_item, parent, false);
        return new CheckBoxIPickerAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        String labelName = mData.get(position).toString();
        ((ViewHolder) holder).checkedTextView.setText(labelName);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public List<T> getCheckedItems() {
        return new ArrayList<>();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView checkedTextView;
        CheckBox checkBox;

        ViewHolder(View itemView) {
            super(itemView);
            checkedTextView = itemView.findViewById(R.id.checkbox_text);
            checkBox = itemView.findViewById(R.id.checkbox);

            checkedTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkBox.isChecked())
                        checkBox.setChecked(false);
                    else
                        checkBox.setChecked(true);
                }
            });
        }


    }
}
