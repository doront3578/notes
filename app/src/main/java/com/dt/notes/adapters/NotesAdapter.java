package com.dt.notes.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.dt.notes.R;
import com.dt.notes.common.Selectable;
import com.dt.notes.entities.Label;
import com.dt.notes.entities.Note;
import com.dt.notes.viewmodels.NoteViewModel;
import com.dt.notes.viewmodels.SelectedItemsViewModel;
import com.google.android.material.card.MaterialCardView;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <h1>NotesAdapter</h1>
 *
 * <p>
 * A {@link ListAdapter} that is responsible for managing a list view for notes.
 * This adapter is also implementing {@link Selectable}, allowing it to implement a selection mode
 * when items are clicked.
 * </p>
 */
public class NotesAdapter extends ListAdapter<Note, NotesAdapter.NoteHolder> implements Selectable, Filterable {
    private OnItemClickListener mOnItemClickListener;
    private SelectedItemsViewModel mSelectedItemsViewModel;
    private NoteViewModel mNoteViewModel;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Note> mItemsList;

    /**
     * Constructs a new {@link NotesAdapter}.
     *
     * @param selectedItemsViewModel The view model that holds the selected items list.
     * @param onItemClickListener    The listener for item clicks.
     */
    public NotesAdapter(RecyclerView.LayoutManager layoutManager, NoteViewModel noteViewModel, SelectedItemsViewModel selectedItemsViewModel, OnItemClickListener onItemClickListener) {
        super(DIFF_CALLBACK);

        mNoteViewModel = noteViewModel;
        mSelectedItemsViewModel = selectedItemsViewModel;
        mOnItemClickListener = onItemClickListener;
        mLayoutManager = layoutManager;

    }

    private static final DiffUtil.ItemCallback<Note> DIFF_CALLBACK = new DiffUtil.ItemCallback<Note>() {
        @Override
        public boolean areItemsTheSame(@NonNull Note oldItem, @NonNull Note newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Note oldItem, @NonNull Note newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) &&
                    oldItem.getDescription().equals(newItem.getDescription()) &&
                    oldItem.getColor().equals(newItem.getColor());
        }
    };

    @Override
    public void submitList(@Nullable List<Note> list) {
        mItemsList = list;
        super.submitList(list);
    }

    public void submitList(@Nullable List<Note> list, boolean withoutCache) {
        super.submitList(list);
    }

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_item, parent, false);

        return new NoteHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder holder, int position) {
        Note currentNote = getItem(position);

        holder.title.setText(currentNote.getTitle());
        holder.description.setText(currentNote.getDescription());

        Date date = currentNote.getCreateDate();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.get(Calendar.MONTH);
        String dateString = getMonthForInt(c.get(Calendar.MONTH)) + ", " + c.get(Calendar.DAY_OF_MONTH);

        holder.date.setText(dateString);
        holder.setBackGroundColor(currentNote.getColor());
        holder.bindOnClickListener(mOnItemClickListener);

        toggleItemSelectionStyle(holder, position);
    }

    /**
     * @param position Position of the item.
     * @return The note at the specified position.
     */
    public Note getNoteAt(int position) {
        return getItem(position);
    }

    /**
     * <h1>NoteHolder</h1>
     *
     * <p>
     * a {@link androidx.recyclerview.widget.RecyclerView.ViewHolder} for a note object.
     * </p>
     */
    @SuppressWarnings("WeakerAccess")
    public static class NoteHolder extends RecyclerView.ViewHolder {
        public MaterialCardView container;
        public CardView background1;
        public CardView background2;
        private TextView title;
        private TextView description;
        private TextView date;

        private NoteHolder(View itemView) {
            super(itemView);

            background1 = itemView.findViewById(R.id.action1_view);
            background2 = itemView.findViewById(R.id.action2_view);
            container = itemView.findViewById(R.id.details_view);
            title = itemView.findViewById(R.id.tv_title);
            description = itemView.findViewById(R.id.tv_description);
            ImageView indicator = itemView.findViewById(R.id.indicator);
            date = itemView.findViewById(R.id.note_create_date);
        }

        private void setBackGroundColor(String color) {
            try {
                container.setCardBackgroundColor(Color.parseColor(color));
            } catch (Exception e) {
                container.setStrokeColor(Color.WHITE);
            }
        }

        /**
         * Binds this view to an on click listener, supplying it with click events.
         *
         * @param clickListener The listener to bind to.
         */
        private void bindOnClickListener(final OnItemClickListener clickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    if (clickListener != null && position != RecyclerView.NO_POSITION)
                        clickListener.onItemClick(position);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int position = getAdapterPosition();

                    if (clickListener != null && position != RecyclerView.NO_POSITION) {
                        clickListener.onItemLongClick(position);

                        return true;
                    }

                    return false;
                }
            });
        }
    }

    @Override
    public void toggleItemSelection(int pos) {
        if (mSelectedItemsViewModel.selectedItems.get(pos, false))
            mSelectedItemsViewModel.selectedItems.delete(pos);
        else
            mSelectedItemsViewModel.selectedItems.put(pos, true);

        // Giving a change to the item view to change style style according it's selection status.
        notifyItemChanged(pos, mLayoutManager.getChildAt(pos - 1));
    }

    /**
     * Updates the style of an item according the it's selection status.
     *
     * @param holder   The holder to access view.
     * @param position Possible position of the view in the selected items list.
     */
    private void toggleItemSelectionStyle(NoteHolder holder, int position) {
        if (mSelectedItemsViewModel.selectedItems.get(position, false)) {
            holder.container.setStrokeWidth(4);
            holder.container.setStrokeColor(Color.BLACK);
        } else
            holder.container.setStrokeWidth(0);
    }


    @Override
    public List<Integer> getSelectedItemsPositions() {
        List<Integer> selectedItemsPositions = new ArrayList<>(mSelectedItemsViewModel.selectedItems.size());

        for (int itemIndex = 0; itemIndex < mSelectedItemsViewModel.selectedItems.size(); itemIndex++)
            selectedItemsPositions.add(mSelectedItemsViewModel.selectedItems.keyAt(itemIndex));

        return selectedItemsPositions;
    }

    @Override
    public void clearSelections() {
        for (int i = 0; i < mSelectedItemsViewModel.selectedItems.size(); i++) {
            int key = mSelectedItemsViewModel.selectedItems.keyAt(i);

            if (mSelectedItemsViewModel.selectedItems.get(key)) {
                try {
                    notifyItemChanged(key, getNoteAt(key));
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        }

        mSelectedItemsViewModel.selectedItems.clear();
    }

    @Override
    public int getSelectedItemsCount() {
        return mSelectedItemsViewModel.selectedItems.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NotNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public Filter getFilter() {
        return notesFilter;
    }


    private Filter notesFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Note> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(mItemsList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Note note : mItemsList) {
                    boolean labelsIncludeSequence = false;
                    List<Label> labels = mNoteViewModel.getNoteLabelsSync(note.getId()).labels;

                    if (labels != null) {
                        for (Label l : labels) {
                            if (l.getName().toLowerCase().contains(filterPattern)) {
                                labelsIncludeSequence = true;
                                break;
                            }
                        }
                    }

                    if (note.getTitle().toLowerCase().contains(filterPattern) || note.getDescription().toLowerCase().contains(filterPattern) || labelsIncludeSequence) {
                        filteredList.add(note);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            submitList((List<Note>) results.values, true);
        }
    };

    String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }

    /**
     * <h1>OnItemClickListener</h1>
     *
     * <p>
     * A simple listener interface to handle click events of a view in the adapter.
     * </p>
     */
    public interface OnItemClickListener {
        /**
         * Called after an item was clicked
         *
         * @param position Position of the item within the adapter
         */
        void onItemClick(int position);

        /**
         * Called after an item was long clicked
         *
         * @param position Position of the item within the adapter
         */
        void onItemLongClick(int position);
    }
}