package com.dt.notes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dt.notes.R;
import com.dt.notes.util.CustomSpinnerItem;

import java.util.ArrayList;
import java.util.List;

public class CustomSpinnerAdapter extends ArrayAdapter<CustomSpinnerItem> {
    public CustomSpinnerAdapter(Context context, List<CustomSpinnerItem> itemList) {
        super(context, 0, itemList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_item, parent, false);
        }

        TextView primaryText = convertView.findViewById(R.id.primaryText);
        TextView secondaryText = convertView.findViewById(R.id.secondaryText);

        CustomSpinnerItem currentItem = getItem(position);

        primaryText.setText(currentItem.getmItemDispName());
        secondaryText.setText(currentItem.getmSecondaryData());

        return convertView;
    }
}
