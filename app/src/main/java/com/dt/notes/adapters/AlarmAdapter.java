package com.dt.notes.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.dt.notes.R;
import com.dt.notes.entities.Alarm;
import com.dt.notes.entities.Note;
import com.dt.notes.viewmodels.NoteViewModel;
import com.dt.notes.viewmodels.SelectedItemsViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AlarmAdapter extends ListAdapter<Alarm, AlarmAdapter.AlarmHolder> {
    private AlarmAdapter.OnItemClickListener mListener;
    private SelectedItemsViewModel mSelectedItemsViewModel;
    private NoteViewModel mNoteViewModel;

    public AlarmAdapter(SelectedItemsViewModel viewModel, NoteViewModel noteViewModel) {
        super(DIFF_CALLBACK);

        mListener = null;
        mSelectedItemsViewModel = viewModel;
        mNoteViewModel = noteViewModel;
    }

    private static final DiffUtil.ItemCallback<Alarm> DIFF_CALLBACK = new DiffUtil.ItemCallback<Alarm>() {
        @Override
        public boolean areItemsTheSame(@NonNull Alarm oldItem, @NonNull Alarm newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Alarm oldItem, @NonNull Alarm newItem) {
            return oldItem.getAlarmTime() == newItem.getAlarmTime() &&
                    oldItem.getNoteId() == newItem.getNoteId();
        }
    };

    @NonNull
    @Override
    public AlarmHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.alarm_item, parent, false);

        return new AlarmHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmHolder holder, int position) {
        Alarm currentAlarm = getItem(position);
        Note note = mNoteViewModel.get(currentAlarm.getNoteId());

        holder.title.setText(note.getTitle());

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(currentAlarm.getAlarmTime());

        holder.alarmTime.setText(String.format("%1$tA %1$tb %1$td %1$tY at %1$tI:%1$tM %1$Tp", calendar));
        holder.bindOnClickListener(mListener);

        toggleCheckedIcon(holder, position);
    }

    public Alarm getAlarmAt(int position) {
        return getItem(position);
    }

    class AlarmHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView alarmTime;

        public AlarmHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.alarm_title);
            alarmTime = itemView.findViewById(R.id.alarm_time);
        }

        public void setBackGroundColor(String color) {
            try {
                ((CardView) itemView).setCardBackgroundColor(Color.parseColor(color));
            } catch (Exception e) {
                ((CardView) itemView).setCardBackgroundColor(Color.WHITE);
            }
        }

        public void bindOnClickListener(final OnItemClickListener clickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    if (clickListener != null && position != RecyclerView.NO_POSITION)
                        clickListener.onItemClick(position);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int position = getAdapterPosition();

                    if (clickListener != null && position != RecyclerView.NO_POSITION) {
                        clickListener.onItemLongClick(position);

                        return true;
                    }

                    return false;
                }
            });
        }
    }

    public void toggleSelection(int pos) {
        if (mSelectedItemsViewModel.selectedItems.get(pos, false)) {
            mSelectedItemsViewModel.selectedItems.delete(pos);
        } else {
            mSelectedItemsViewModel.selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    private void toggleCheckedIcon(AlarmHolder holder, int position) {
        if (mSelectedItemsViewModel.selectedItems.get(position, false)) {
            //holder.indicator.setVisibility(View.VISIBLE);
        } else {
            //holder.indicator.setVisibility(View.GONE);
        }
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(mSelectedItemsViewModel.selectedItems.size());
        for (int i = 0; i < mSelectedItemsViewModel.selectedItems.size(); i++) {
            items.add(mSelectedItemsViewModel.selectedItems.keyAt(i));
        }
        return items;
    }

    public void clearSelections() {
        mSelectedItemsViewModel.selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return mSelectedItemsViewModel.selectedItems.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface OnItemClickListener {
        void onItemClick(int alarmPosition);

        void onItemLongClick(int alarmPosition);
    }
}

