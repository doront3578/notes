package com.dt.notes.config;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Configuration</h1>
 *
 * <p>
 * Handles access to the configuration file of the application and
 * Exports methods to retrieve data from it.
 * </p>
 */
public abstract class Configuration {
    private final static String CONFIGURATION_FILE = "configuration.json";
    private final static String COLORS_ARRAY = "colors";
    private final static String LABELS_ARRAY = "defaultLabels";

    /**
     * Reads the Color codes from the configuration files
     *
     * @param context The context use to open assets
     * @return The list of colors
     * @throws Exception If the read operation failed
     */
    public static List<String> getColors(Context context) throws Exception {
        byte[] configurationsFile = getRawConfigurations(context);

        JSONObject configRootObject = new JSONObject(new String(configurationsFile, StandardCharsets.UTF_8));
        JSONArray colorsArray = configRootObject.getJSONArray(COLORS_ARRAY);

        List<String> colorsList = new ArrayList<>();

        // Inserting the color codes to the list
        for (int i = 0; i < colorsArray.length(); i++) {
            colorsList.add(colorsArray.getString(i));
        }

        return colorsList;
    }

    public static List<String> getDefaultLabels(Context context) throws Exception {
        byte[] configurationFile = getRawConfigurations(context);

        JSONObject configRootObject = new JSONObject(new String(configurationFile, StandardCharsets.UTF_8));
        JSONArray labels = configRootObject.getJSONArray(LABELS_ARRAY);

        List<String> labelsList = new ArrayList<>();

        for (int i = 0; i < labels.length(); i++) {
            labelsList.add(labels.getString(i));
        }

        return labelsList;
    }

    /**
     * Reads the configuration file and returns its content raw.
     *
     * @param context The context use to open assets
     * @return The raw file content in bytes array
     * @throws Exception If the stream returns no data
     */
    private static byte[] getRawConfigurations(Context context) throws Exception {
        InputStream inputStream = context.getAssets().open(CONFIGURATION_FILE);
        int size = inputStream.available();
        byte[] buffer = new byte[size];

        int byteRead = inputStream.read(buffer);
        inputStream.close();

        if (byteRead == -1) {
            throw new Exception("Could'nt read configurations");
        }

        return buffer;
    }
}