package com.dt.notes.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import com.dt.notes.application.Application;
import com.dt.notes.entities.Note;
import com.dt.notes.repositories.AlarmsRepository;
import com.dt.notes.util.NotificationHelper;

import java.util.concurrent.ExecutionException;

public class AlertReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationHelper notificationHelper = new NotificationHelper(context);

        try {
            Note n = new AlarmsRepository((Application) context.getApplicationContext()).getNoteByAlarmId(intent.getIntExtra("alarmId", -1));

            if(n != null) {
                NotificationCompat.Builder nb = notificationHelper.getRemindersChannelNotification(n.getTitle(), n.getDescription());
                notificationHelper.getNotificationManager().notify(intent.getIntExtra("alarmId", -1), nb.build());
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            NotificationCompat.Builder nb = notificationHelper.getRemindersChannelNotification("HI", "There");
            notificationHelper.getNotificationManager().notify(1, nb.build());
        }

        new AlarmsRepository((Application) context.getApplicationContext()).deleteById(intent.getIntExtra("alarmId", -1));
    }
}
