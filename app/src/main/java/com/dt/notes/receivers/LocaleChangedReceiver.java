package com.dt.notes.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dt.notes.util.NotificationHelper;

public class LocaleChangedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().compareTo(Intent.ACTION_LOCALE_CHANGED) == 0) {
            new NotificationHelper(context);
        }
    }
}
