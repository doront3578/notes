package com.dt.notes.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.dt.notes.entities.Note;
import com.dt.notes.entities.NoteLabels;

import java.util.List;

@Dao
public interface NoteDAO {
    @Insert
    long insert(Note note);

    @Update
    void update(Note note);

    @Delete
    void delete(Note note);

    @Query("UPDATE notes SET isDeleted = 1 WHERE isDeleted = 0")
    void moveAllToTrash();

    @Query("SELECT * FROM notes WHERE notes.isDeleted = 0 AND notes.isPrivate = 0 ORDER BY createDate DESC")
    LiveData<List<Note>> getAllExisting();

    @Query("SELECT * FROM notes WHERE notes.isDeleted = 0 AND notes.isPrivate = 1 ORDER BY createDate DESC")
    LiveData<List<Note>> getAllPrivate();

    @Query("SELECT * FROM notes WHERE notes.isDeleted = 1 ORDER BY createDate DESC")
    LiveData<List<Note>> getAllDeleted();

    @Query("SELECT * from notes WHERE noteId = :id LIMIT 1")
    Note get(int id);

    @Transaction
    @Query("SELECT * FROM notes WHERE notes.noteId = :noteId")
    LiveData<NoteLabels> getNoteLabels(int noteId);

    @Transaction
    @Query("SELECT * FROM notes WHERE notes.noteId = :noteId")
    NoteLabels getNoteLabelsSync(int noteId);
}
