package com.dt.notes.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dt.notes.entities.Alarm;
import com.dt.notes.entities.Note;

import java.util.List;

@Dao
public interface AlarmDAO {
    @Insert
    long insert(Alarm alarm);

    @Update
    void update(Alarm alarm);

    @Delete
    void delete(Alarm alarm);

    @Query("DELETE FROM alarms")
    void deleteAll();

    @Query("SELECT * FROM alarms")
    LiveData<List<Alarm>> getAll();

    @Query("SELECT * from alarms where id = :id LIMIT 1")
    Alarm get(int id);

    @Query("SELECT * from alarms where noteId = :noteId")
    Alarm getByNote(int noteId);

    @Query("DELETE FROM alarms WHERE id = :id")
    void deleteById(int id);

    @Query("SELECT notes.* FROM alarms INNER JOIN notes ON alarms.noteId = notes.noteId WHERE (alarms.id = :alarmId AND notes.isDeleted = 0) LIMIT 1")
    Note getNoteByAlarmId(int alarmId);
}
