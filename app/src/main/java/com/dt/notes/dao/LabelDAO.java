package com.dt.notes.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dt.notes.entities.Label;

import java.util.List;

@Dao
public interface LabelDAO {
    @Insert
    long insert(Label label);

    @Delete
    void delete(Label label);

    @Query("DELETE FROM labels WHERE labelId = :id")
    void deleteById(int id);

    @Update
    void update(Label label);

    @Query("SELECT * FROM labels")
    LiveData<List<Label>> getAll();

    @Query("SELECT * FROM labels")
    List<Label> getAllSync();
}
