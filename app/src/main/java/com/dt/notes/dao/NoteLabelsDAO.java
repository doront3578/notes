package com.dt.notes.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.dt.notes.entities.NoteLabelsCrossRef;

@Dao
public interface NoteLabelsDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(NoteLabelsCrossRef noteLabelsCrossRef);

    @Query("DELETE FROM notelabelscrossref WHERE noteId = :noteId")
    void clearLabelsOfNote(int noteId);

    @Query("DELETE FROM notelabelscrossref WHERE noteId = :noteId AND labelId = :labelId")
    void deleteLabelFromNote(int noteId, int labelId);
}
