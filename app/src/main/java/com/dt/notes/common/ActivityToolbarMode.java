package com.dt.notes.common;

/**
 * Represents the current UI mode of an activity
 */
public enum ActivityToolbarMode {
    /**
     * The main toolbar with basic behavior
     */
    NORMAL,

    /**
     * Presents a contextual action bar with a multi selection interface.
     */
    MULTI_SELECTION,

    /**
     * Presents a toolbar with search bar
     */
    SEARCH
}
