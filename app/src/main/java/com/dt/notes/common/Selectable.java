package com.dt.notes.common;

import java.util.List;

/**
 * An Interface used to create a unified contract for managing a selected group of items within a
 * list of data.
 */
public interface Selectable {
    /**
     * Toggle the selection (selected/unselected) of a an item.
     *
     * @param position The position of the item within the list.
     */
    void toggleItemSelection(int position);

    /**
     * @return A list of selected items positions.
     */
    List<Integer> getSelectedItemsPositions();

    /**
     * Clears the selections of the list.
     */
    void clearSelections();

    /**
     *
     * @return The number of selected items.
     */
    int getSelectedItemsCount();
}
