package com.dt.notes.common;

/**
 * Represents the notes list activity current channel type of notes.
 */
public enum NotesListChannel {
    /**
     * Normal notes (not private or deleted).
     */
    NORMAL,

    /**
     * Private notes (isPrivate = true).
     */
    PRIVATE,

    /**
     * Deleted notes (isDeleted = true).
     */
    DELETED,

    /**
     * Dummy type for null.
     */
    UNSET
}
