package com.dt.notes.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.dt.notes.common.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "notes")
@TypeConverters({DateConverter.class})
public class Note implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "noteId")
    private int id;

    private String title;

    private String description;

    private String color;

    private boolean isDeleted;

    private boolean isPrivate;

    private Date createDate;

    public Note(String title, String description, String color, boolean isDeleted, boolean isPrivate) {
        this.title = title;
        this.description = description;
        this.color = color;
        this.isDeleted = isDeleted;
        this.isPrivate = isPrivate;
        this.createDate = new Date();
    }

    // Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // Getters
    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getColor() {
        return color;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}