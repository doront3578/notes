package com.dt.notes.entities;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "alarms", indices = {@Index(value = {"noteId"})}, foreignKeys = {
        @ForeignKey(
                entity = Note.class,
                parentColumns = "noteId",
                childColumns = "noteId",
                onDelete = ForeignKey.CASCADE
        )
})
public class Alarm {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private int noteId;

    private long alarmTime;

    public Alarm(int noteId, long alarmTime) {
        this.noteId = noteId;
        this.alarmTime = alarmTime;
    }

    // Setters
    public void setId(int id) {
        this.id = id;
    }

    // Getters
    public int getId() {
        return id;
    }

    public int getNoteId() {
        return noteId;
    }

    public long getAlarmTime() {
        return alarmTime;
    }
}
