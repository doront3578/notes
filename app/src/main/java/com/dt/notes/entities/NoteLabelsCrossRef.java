package com.dt.notes.entities;

import androidx.room.Entity;

@Entity(primaryKeys = {"noteId", "labelId"})
public class NoteLabelsCrossRef {
    public long noteId;
    public long labelId;

    public NoteLabelsCrossRef(long noteId, long labelId) {
        this.noteId = noteId;
        this.labelId = labelId;
    }
}
