package com.dt.notes.entities;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class NoteLabels {
    @Embedded
    public Note note;

    @Relation(
            parentColumn = "noteId",
            entityColumn = "labelId",
            associateBy = @Junction(NoteLabelsCrossRef.class)
    )
    public List<Label> labels;
}
