package com.dt.notes.helpers;

import android.animation.AnimatorSet;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.HashMap;
import java.util.Map;

public class ToolbarsManager {
    public static final int NO_ANIMATION = 0;
    public static final int ANIMATION_IN = 1;
    public static final int ANIMATION_OUT = 2;

    private Map<String, Toolbar> mToolbars;
    private AnimatorSet mInAnimations;
    private AnimatorSet mOutAnimations;
    private int mLastAnimation;

    public ToolbarsManager() {
        mToolbars = new HashMap<>();
    }

    public void addToolbar(String name, Toolbar toolbar) {
        mToolbars.put(name, toolbar);
    }

    public Toolbar renderToolbar(AppCompatActivity context, String name, ViewGroup parent, int animationStrategy) {
        Toolbar toolbar = mToolbars.get(name);

        if (toolbar != null) {
            parent.removeAllViews();

            if (mLastAnimation != animationStrategy) {
                switch (animationStrategy) {
                    case ANIMATION_IN:
                        mInAnimations.start();
                        break;
                    case ANIMATION_OUT:
                        mOutAnimations.start();
                        break;
                    default:
                        if (animationStrategy != NO_ANIMATION)
                            throw new IllegalArgumentException("Wrong animation");
                }
            }

            mLastAnimation = animationStrategy;
            parent.addView(toolbar);
            context.setSupportActionBar(toolbar);

            return toolbar;
        }

        return null;
    }

    public void setmInAnimation(AnimatorSet animatorSet) {
        mInAnimations = animatorSet;
    }

    public void setmOutAnimation(AnimatorSet animatorSet) {
        mOutAnimations = animatorSet;
    }
}
