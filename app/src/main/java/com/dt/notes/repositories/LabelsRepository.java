package com.dt.notes.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.dt.notes.dao.LabelDAO;
import com.dt.notes.database.NoteDatabase;
import com.dt.notes.entities.Label;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class LabelsRepository {
    private LabelDAO labelDAO;
    private LiveData<List<Label>> allLabels;

    public LabelsRepository(Application application) {
        NoteDatabase noteDatabase = NoteDatabase.getInstance(application);
        labelDAO = noteDatabase.labelDAO();
        allLabels = labelDAO.getAll();
    }

    public LiveData<List<Label>> getAllLabels() {
        return allLabels;
    }

    public List<Label> getAllLabelsSync() {
        AsyncTask<Void, Void, List<Label>> asyncTask = new GetAllLabelsTaskSync(labelDAO).execute();

        try {
            return asyncTask.get();
        } catch (ExecutionException | InterruptedException e) {
            return null;
        }
    }

    public long insert(Label label) {
        AsyncTask<Label, Void, Long> asyncTask = new InsertLabelAsyncTask(labelDAO).execute(label);

        try {
            return asyncTask.get();
        } catch (ExecutionException | InterruptedException e) {
            return -1;
        }
    }

    public void delete(Label label) {
        new DeleteLabelAsyncTask(labelDAO).execute(label);
    }

    public void deleteById(int id) {
        new DeleteLabelByIdAsyncTask(labelDAO).execute(id);
    }

    public void update(Label label) {
        new UpdateLabelAsyncTask(labelDAO).execute(label);
    }

    private static class InsertLabelAsyncTask extends AsyncTask<Label, Void, Long> {
        private LabelDAO labelDAO;

        private InsertLabelAsyncTask(LabelDAO labelDAO) {
            this.labelDAO = labelDAO;
        }

        @Override
        protected Long doInBackground(Label... labels) {
            return labelDAO.insert(labels[0]);
        }
    }

    private static class GetAllLabelsTaskSync extends AsyncTask<Void, Void, List<Label>> {
        private LabelDAO labelDAO;

        private GetAllLabelsTaskSync(LabelDAO labelDAO) {
            this.labelDAO = labelDAO;
        }

        @Override
        protected List<Label> doInBackground(Void... voids) {
            return labelDAO.getAllSync();
        }
    }

    private static class DeleteLabelAsyncTask extends AsyncTask<Label, Void, Void> {
        private LabelDAO labelDAO;

        private DeleteLabelAsyncTask (LabelDAO labelDAO) {
            this.labelDAO = labelDAO;
        }

        @Override
        protected Void doInBackground(Label... labels) {
            labelDAO.delete(labels[0]);

            return null;
        }
    }

    private static class DeleteLabelByIdAsyncTask extends AsyncTask<Integer, Void, Void> {
        private LabelDAO labelDAO;

        public DeleteLabelByIdAsyncTask(LabelDAO labelDAO) {
            this.labelDAO = labelDAO;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            labelDAO.deleteById(integers[0]);

            return null;
        }
    }

    private static class UpdateLabelAsyncTask extends AsyncTask<Label, Void, Void> {
        private LabelDAO labelDAO;

        public UpdateLabelAsyncTask(LabelDAO labelDAO) {
            this.labelDAO = labelDAO;
        }

        @Override
        protected Void doInBackground(Label... labels) {
            labelDAO.update(labels[0]);

            return null;
        }
    }
}
