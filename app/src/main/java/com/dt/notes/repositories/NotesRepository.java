package com.dt.notes.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.dt.notes.dao.NoteDAO;
import com.dt.notes.dao.NoteLabelsDAO;
import com.dt.notes.database.NoteDatabase;
import com.dt.notes.entities.Note;
import com.dt.notes.entities.NoteLabels;
import com.dt.notes.entities.NoteLabelsCrossRef;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class NotesRepository {
    private NoteDAO noteDAO;
    private NoteLabelsDAO noteLabelsDAO;
    private LiveData<List<Note>> allExistingNotes;
    private LiveData<List<Note>> allDeletedNotes;
    private LiveData<List<Note>> getAllPrivateNotes;

    public NotesRepository(Application application) {
        NoteDatabase database = NoteDatabase.getInstance(application);
        noteDAO = database.noteDAO();
        noteLabelsDAO = database.noteLabelsDAO();
        allExistingNotes = noteDAO.getAllExisting();
        allDeletedNotes = noteDAO.getAllDeleted();
        getAllPrivateNotes = noteDAO.getAllPrivate();
    }

    public long insert(Note note) {
        AsyncTask<Note, Void, Long> asyncTask = new InsertNoteAsyncTask(noteDAO).execute(note);


        try {
            return asyncTask.get();
        } catch (ExecutionException | InterruptedException e) {
            return -1;
        }
    }

    public void update(Note note) {
        new UpdateNoteAsyncTask(noteDAO).execute(note);
    }

    public void delete(Note note) {
        new DeleteNoteAsyncTask(noteDAO).execute(note);
    }

    public void moveToTrash(Note note) {
        note.setIsDeleted(true);
        new UpdateNoteAsyncTask(noteDAO).execute(note);
    }

    public void restoreFromTrash(Note note) {
        note.setIsDeleted(false);
        new UpdateNoteAsyncTask(noteDAO).execute(note);
    }

    public LiveData<List<Note>> getAllPrivate() {
        return getAllPrivateNotes;
    }

    public void moveAllNotesToTrash() {
        new MoveAllNotesToTrash(noteDAO).execute();
    }

    public LiveData<List<Note>> getAllDeleted() {
        return allDeletedNotes;
    }

    public LiveData<List<Note>> getAllExisting() {
        return allExistingNotes;
    }

    public Note get(int id) {
        AsyncTask<Integer, Void, Note> a = new GetNoteAsyncTask(noteDAO).execute(id);
        try {
            return a.get();
        } catch (Exception e) {
            return null;
        }
    }

    public LiveData<NoteLabels> getNoteLabels(int noteId) {
        AsyncTask<Integer, Void, LiveData<NoteLabels>> a = new getNoteLablesAsyncTask(noteDAO).execute(noteId);
        try {
            return a.get();
        } catch (Exception e) {
            return null;
        }
    }

    public NoteLabels getNoteLabelsSync(int noteId) {
        AsyncTask<Integer, Void, NoteLabels>labels = new getNoteLablesAsyncTaskSync(noteDAO).execute(noteId);

        try {
            return labels.get();
        } catch (Exception e) {
            return null;
        }
    }

    public void addLabelToNote(int noteId, int labelId) {
        new addLabelToNote(noteLabelsDAO).execute(noteId, labelId);
    }

    public void deleteLabelsOfNote(int noteId) {
        new deleteLabelsOfNote(noteLabelsDAO).execute(noteId);
    }

    public void removeLabelFromNote(int noteId, int labelId) {
        new removeLabelFromNoteAsyncTask(noteLabelsDAO).execute(noteId, labelId);
    }

    private static class removeLabelFromNoteAsyncTask extends AsyncTask<Integer,Void, Void> {
        private NoteLabelsDAO noteLabelsDAO;

        removeLabelFromNoteAsyncTask(NoteLabelsDAO noteLabelsDAO) {
            this.noteLabelsDAO = noteLabelsDAO;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            this.noteLabelsDAO.deleteLabelFromNote(integers[0], integers[1]);

            return null;
        }
    }

    private static class deleteLabelsOfNote extends AsyncTask<Integer, Void, Void> {
        private NoteLabelsDAO noteLabelsDAO;

        deleteLabelsOfNote(NoteLabelsDAO noteLabelsDAO) {
            this.noteLabelsDAO = noteLabelsDAO;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            this.noteLabelsDAO.clearLabelsOfNote(integers[0]);

            return null;
        }
    }

    private static class addLabelToNote extends AsyncTask<Integer, Void, Void> {
        private NoteLabelsDAO noteLabelsDAO;

        private addLabelToNote (NoteLabelsDAO noteLabelsDAO) {
            this.noteLabelsDAO = noteLabelsDAO;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            noteLabelsDAO.insert(new NoteLabelsCrossRef(integers[0], integers[1]));

            return null;
        }
    }

    private static class getNoteLablesAsyncTask extends AsyncTask<Integer, Void, LiveData<NoteLabels>> {
        private NoteDAO noteDAO;

        private getNoteLablesAsyncTask(NoteDAO noteDAO) {
            this.noteDAO = noteDAO;
        }

        @Override
        protected LiveData<NoteLabels> doInBackground(Integer... integers) {
            return noteDAO.getNoteLabels(integers[0]);
        }
    }

    private static class getNoteLablesAsyncTaskSync extends AsyncTask<Integer, Void, NoteLabels> {
        private NoteDAO noteDAO;

        private getNoteLablesAsyncTaskSync(NoteDAO noteDAO) {
            this.noteDAO = noteDAO;
        }

        @Override
        protected NoteLabels doInBackground(Integer... integers) {
            return noteDAO.getNoteLabelsSync(integers[0]);
        }
    }

    private static class InsertNoteAsyncTask extends AsyncTask<Note, Void, Long> {
        private NoteDAO noteDAO;

        private InsertNoteAsyncTask(NoteDAO noteDAO) {
            this.noteDAO = noteDAO;
        }

        @Override
        protected Long doInBackground(Note... notes) {
            return noteDAO.insert(notes[0]);
        }
    }

    private static class UpdateNoteAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDAO noteDAO;

        private UpdateNoteAsyncTask(NoteDAO noteDAO) {
            this.noteDAO = noteDAO;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteDAO.update(notes[0]);

            return null;
        }
    }

    private static class DeleteNoteAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDAO noteDAO;

        private DeleteNoteAsyncTask(NoteDAO noteDAO) {
            this.noteDAO = noteDAO;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteDAO.delete(notes[0]);

            return null;
        }
    }

    private static class MoveAllNotesToTrash extends AsyncTask<Void, Void, Void> {
        private NoteDAO noteDAO;

        private MoveAllNotesToTrash(NoteDAO noteDAO) {
            this.noteDAO = noteDAO;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            noteDAO.moveAllToTrash();

            return null;
        }
    }

    private static class GetNoteAsyncTask extends AsyncTask<Integer, Void, Note> {
        private NoteDAO noteDAO;

        private GetNoteAsyncTask(NoteDAO noteDAO) {
            this.noteDAO = noteDAO;
        }

        @Override
        protected Note doInBackground(Integer... ints) {
            return noteDAO.get(ints[0]);
        }
    }
}
