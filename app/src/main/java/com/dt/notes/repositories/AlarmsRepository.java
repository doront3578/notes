package com.dt.notes.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.dt.notes.dao.AlarmDAO;
import com.dt.notes.database.NoteDatabase;
import com.dt.notes.entities.Alarm;
import com.dt.notes.entities.Note;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class AlarmsRepository {
    private AlarmDAO alarmDAO;
    private LiveData<List<Alarm>> allAlarms;

    public AlarmsRepository(Application application) {
        NoteDatabase database = NoteDatabase.getInstance(application);
        alarmDAO = database.alarmDAO();
        allAlarms = alarmDAO.getAll();
    }

    public long insert(Alarm alarm) throws ExecutionException, InterruptedException {
        return new InsertAlarmAsyncTask(alarmDAO).execute(alarm).get();
    }

    public void update(Alarm alarm) {
        new UpdateAlarmAsyncTask(alarmDAO).execute(alarm);
    }

    public void delete(Alarm alarm) {
        new DeleteAlarmAsyncTask(alarmDAO).execute(alarm);
    }

    public void deleteById(int id) {
        new DeleteAlarmByIdAsyncTask(alarmDAO).execute(id);
    }

    public void deleteAll() {
        new DeleteAllAlarmsAsyncTask(alarmDAO).execute();
    }

    public LiveData<List<Alarm>> getAll() {
        return allAlarms;
    }

    public Note getNoteByAlarmId(int alarmId) throws ExecutionException, InterruptedException {
        return new GetNoteByAlarmIdAsyncTask(alarmDAO).execute(alarmId).get();
    }

    public Alarm get(int id) {
        AsyncTask<Integer, Void, Alarm> a = new GetAlarmAsyncTask(alarmDAO).execute(id);
        try {
            return a.get();
        } catch (Exception e) {
            return null;
        }
    }

    public Alarm getByNote(int noteId) {
        AsyncTask<Integer, Void, Alarm> a = new GetAlarmByNoteAsyncTask(alarmDAO).execute(noteId);
        try {
            return a.get();
        } catch (Exception e) {
            return null;
        }
    }

    private static class InsertAlarmAsyncTask extends AsyncTask<Alarm, Void, Long> {
        private AlarmDAO alarmDao;

        private InsertAlarmAsyncTask(AlarmDAO alarmDao) {
            this.alarmDao = alarmDao;
        }

        @Override
        protected Long doInBackground(Alarm... alarms) {
            return alarmDao.insert(alarms[0]);
        }
    }

    private static class UpdateAlarmAsyncTask extends AsyncTask<Alarm, Void, Void> {
        private AlarmDAO alarmDao;

        private UpdateAlarmAsyncTask(AlarmDAO alarmDao) {
            this.alarmDao = alarmDao;
        }

        @Override
        protected Void doInBackground(Alarm... alarms) {
            alarmDao.update(alarms[0]);

            return null;
        }
    }

    private static class DeleteAlarmAsyncTask extends AsyncTask<Alarm, Void, Void> {
        private AlarmDAO alarmDAO;

        private DeleteAlarmAsyncTask(AlarmDAO alarmDAO) {
            this.alarmDAO = alarmDAO;
        }

        @Override
        protected Void doInBackground(Alarm... alarms) {
            alarmDAO.delete(alarms[0]);

            return null;
        }
    }

    private static class GetNoteByAlarmIdAsyncTask extends AsyncTask<Integer, Void, Note> {
        private AlarmDAO alarmDAO;

        private GetNoteByAlarmIdAsyncTask(AlarmDAO alarmDAO) {
            this.alarmDAO = alarmDAO;
        }

        @Override
        protected Note doInBackground(Integer... integers) {
            return alarmDAO.getNoteByAlarmId(integers[0]);
        }
    }

    private static class DeleteAlarmByIdAsyncTask extends AsyncTask<Integer, Void, Void> {
        private AlarmDAO alarmDAO;

        private DeleteAlarmByIdAsyncTask(AlarmDAO alarmDAO) {
            this.alarmDAO = alarmDAO;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            alarmDAO.deleteById(integers[0]);

            return null;
        }
    }

    private static class DeleteAllAlarmsAsyncTask extends AsyncTask<Void, Void, Void> {
        private AlarmDAO alarmDAO;

        private DeleteAllAlarmsAsyncTask(AlarmDAO alarmDAO) {
            this.alarmDAO = alarmDAO;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            alarmDAO.deleteAll();

            return null;
        }
    }

    private static class GetAlarmAsyncTask extends AsyncTask<Integer, Void, Alarm> {
        private AlarmDAO alarmDAO;

        private GetAlarmAsyncTask(AlarmDAO alarmDAO) {
            this.alarmDAO = alarmDAO;
        }

        @Override
        protected Alarm doInBackground(Integer... ints) {
            return alarmDAO.get(ints[0]);
        }
    }

    private static class GetAlarmByNoteAsyncTask extends AsyncTask<Integer, Void, Alarm> {
        private AlarmDAO alarmDAO;

        private GetAlarmByNoteAsyncTask(AlarmDAO alarmDAO) {
            this.alarmDAO = alarmDAO;
        }

        @Override
        protected Alarm doInBackground(Integer... ints) {
            return alarmDAO.getByNote(ints[0]);
        }
    }
}
