package com.dt.notes.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dt.notes.R;
import com.dt.notes.adapters.LabelsAdapter;
import com.dt.notes.entities.Label;
import com.dt.notes.viewmodels.LabelsViewModel;

import java.util.ArrayList;
import java.util.List;

public class LabelsActivity extends AppCompatActivity {
    private List<Label> mLabels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_labels);

        mLabels = new ArrayList<>();
        initViews();
    }

    private void initViews() {
        try {
            final LabelsViewModel labelsViewModel = ViewModelProviders.of(this).get(LabelsViewModel.class);

            RecyclerView rv = findViewById(R.id.labels_rv);
            rv.setLayoutManager(new LinearLayoutManager(this));
            final LabelsAdapter adapter = new LabelsAdapter(mLabels);
            rv.setAdapter(adapter);

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv.getContext(), DividerItemDecoration.VERTICAL);
            rv.addItemDecoration(dividerItemDecoration);

            ImageButton addLabelButton = findViewById(R.id.add_label_btn);
            final EditText addLabelText = findViewById(R.id.add_label_tv);

            addLabelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String text = addLabelText.getText().toString();

                    if (text.equals("")) {
                        Toast.makeText(LabelsActivity.this, "Enter label name", Toast.LENGTH_SHORT).show();
                    } else {
                        labelsViewModel.insert(new Label(text));
                    }
                }
            });

            labelsViewModel.getAllLabels().observe(this, new Observer<List<Label>>() {
                @Override
                public void onChanged(List<Label> labels) {
                    mLabels = labels;
                    adapter.setData(mLabels);

                    String[] list = new String[labels.size()];
                    for (int i = 0; i < labels.size(); i++) {
                        list[i] = labels.get(i).getName();
                    }

                    //SimpleCheckBoxPickerFragment<Label> checkBoxPickerFragment = new SimpleCheckBoxPickerFragment<>(mLabels);
                    //checkBoxPickerFragment.show(getSupportFragmentManager(), "gg");

                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(LabelsActivity.this);
                    mBuilder.setTitle("labels");
                    mBuilder.setMultiChoiceItems(list, new boolean[mLabels.size()], new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
    //                        if (isChecked) {
    //                            if (!mUserItems.contains(position)) {
    //                                mUserItems.add(position);
    //                            }
    //                        } else if (mUserItems.contains(position)) {
    //                            mUserItems.remove(position);
    //                        }
    //                        if(isChecked){
    //                            mUserItems.add(position);
    //                        }else{
    //                            mUserItems.remove((Integer.valueOf(position)));
    //                        }
                        }
                    });

                    mBuilder.setCancelable(false);
                    mBuilder.setPositiveButton("save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            String item = "";
    //                        for (int i = 0; i < mUserItems.size(); i++) {
    //                            item = item + listItems[mUserItems.get(i)];
    //                            if (i != mUserItems.size() - 1) {
    //                                item = item + ", ";
    //                            }
    //                        }
    //                        mItemSelected.setText(item);
                        }
                    });

                    mBuilder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    mBuilder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
    //                        for (int i = 0; i < checkedItems.length; i++) {
    //                            checkedItems[i] = false;
    //                            mUserItems.clear();
    //                            mItemSelected.setText("");
    //                        }
                        }
                    });

                    AlertDialog mDialog = mBuilder.create();
                    //mDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
