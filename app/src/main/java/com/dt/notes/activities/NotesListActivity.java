package com.dt.notes.activities;

import android.animation.AnimatorSet;
import android.animation.LayoutTransition;
import android.animation.ValueAnimator;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.biometric.BiometricPrompt;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.dt.notes.R;
import com.dt.notes.adapters.NotesAdapter;
import com.dt.notes.common.ActivityToolbarMode;
import com.dt.notes.common.NotesListChannel;
import com.dt.notes.config.Configuration;
import com.dt.notes.entities.Alarm;
import com.dt.notes.entities.Label;
import com.dt.notes.entities.Note;
import com.dt.notes.fragments.ColorPickerFragment;
import com.dt.notes.helpers.RecyclerItemTouchHelper;
import com.dt.notes.helpers.ToolbarsManager;
import com.dt.notes.receivers.AlertReceiver;
import com.dt.notes.recyclerviews.NotesRecyclerView;
import com.dt.notes.viewmodels.AlarmsViewModel;
import com.dt.notes.viewmodels.LabelsViewModel;
import com.dt.notes.viewmodels.NoteViewModel;
import com.dt.notes.viewmodels.NotesListActivityViewModel;
import com.dt.notes.viewmodels.SelectedItemsViewModel;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;

/**
 * <h1>NotesListActivity</h1>
 *
 * <p>
 * The main activity of the application.
 * Responsible for showing the notes list and handling key operations like
 * edit and delete notes.
 * </p>
 */
public class NotesListActivity extends AppCompatActivity implements NotesAdapter.OnItemClickListener, ColorPickerFragment.ColorPickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    // region Constants
    public static final int ADD_NOTE_REQUEST = 1;
    public static final int EDIT_NOTE_REQUEST = 2;
    private static final String COLOR_PICKER_FRAGMENT_TAG = "NotesListActivity.ColorPickerFragment";
    private static final String TOOLBAR_MAIN = "NotesListActivity.TOOLBAR_MAIN";
    private static final String TOOLBAR_SEARCH = "NotesListActivity.TOOLBAR_SEARCH";
    private static final String TOOLBAR_MULTI_SELECTION = "NotesListActivity.TOOLBAR_MULTI_SELECTION";
    // endregion

    // region Members
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mDrawerToggle;
    private FloatingActionButton mAddNoteFabButton;
    private NotesRecyclerView mNotesListRecyclerView;
    private NotesAdapter mNotesAdapter;
    private NoteViewModel mNoteViewModel;
    private NotesListActivityViewModel mActivityViewModel;
    private LabelsViewModel mLabelsViewModel;
    private AlarmsViewModel mAlarmsViewModel;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private MaterialCardView mToolbarContainer;
    private List<Label> mAllLabels;
    private AlertDialog mDialog;
    // endregion

    // region Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initBiometricAuthentication();
    }

    /**
     * Initiating view related objects like the Toolbar and the Recycler View.
     */
    private void initViews() {
        final NavigationView mNavigationView = findViewById(R.id.nav_view);

        Toolbar mToolbar = findViewById(R.id.toolbar);
        mDrawer = findViewById(R.id.drawer_layout);
        mAddNoteFabButton = findViewById(R.id.button_add_note);

        mNotesListRecyclerView = findViewById(R.id.recycler_view);
        SelectedItemsViewModel selectedItemsViewModel = ViewModelProviders.of(this).get(SelectedItemsViewModel.class);

        // ViewModel
        mNoteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        mActivityViewModel = ViewModelProviders.of(this).get(NotesListActivityViewModel.class);
        mAlarmsViewModel = ViewModelProviders.of(this).get(AlarmsViewModel.class);
        mLabelsViewModel = ViewModelProviders.of(this).get(LabelsViewModel.class);

        // RecyclerView
        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(mActivityViewModel.getRecyclerViewLayoutSpan(), StaggeredGridLayoutManager.VERTICAL);
        mNotesListRecyclerView.setLayoutManager(mLayoutManager);
        mNotesAdapter = new NotesAdapter(mNotesListRecyclerView.getLayoutManager(), mNoteViewModel, selectedItemsViewModel, this);
        mNotesListRecyclerView.setLayoutManager(mLayoutManager);

        mNotesListRecyclerView.setAdapter(mNotesAdapter);
        mNotesListRecyclerView.scrollToPosition(0);
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.END | ItemTouchHelper.START, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mNotesListRecyclerView);

        // Toolbar
        setSupportActionBar(mToolbar);
        mToolbar.setLayoutTransition(new LayoutTransition());
        mToolbarContainer = findViewById(R.id.toolbar_card_wrapper);
        initToolbarsManager();

        // Drawer
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.navigation_draw_open, R.string.navigation_draw_close);
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        switch (mActivityViewModel.getMode()) {
            case NORMAL:
                startNormalMode();
                break;
            case MULTI_SELECTION:
                startMultiSelectionMode();
                break;
            case SEARCH:
                startSearchMode();
                break;
        }

        // Starts the AddEditNoteActivity when the add icon is clicked
        mAddNoteFabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startActivityIntent = new Intent(NotesListActivity.this, AddEditNoteActivity.class);
                startActivityForResult(startActivityIntent, ADD_NOTE_REQUEST);
            }
        });

        // Observing the labels
        mLabelsViewModel.getAllLabels().observe(this, new Observer<List<Label>>() {
            @Override
            public void onChanged(List<Label> labels) {
                mAllLabels = labels;
            }
        });

        // Observing the current channel and changing activity behavior accordingly
        mActivityViewModel.getCurrentChannel().observeForever(new Observer<NotesListChannel>() {
            @Override
            public void onChanged(NotesListChannel notesListChannel) {
                if (notesListChannel.equals(NotesListChannel.UNSET))
                    mActivityViewModel.setCurrentChannel(NotesListChannel.NORMAL);

                loadDataOnCurrentChannel(notesListChannel);
            }
        });

        // Setting a listener to when a navigation view item is clicked
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_reminders:
                        Intent i = new Intent(NotesListActivity.this, AlarmsActivity.class);
                        startActivity(i);

                        break;
                    case R.id.nav_private_notes:
                        NotesListChannel channel = mActivityViewModel.getCurrentChannel().getValue();

                        if (channel != null && !mActivityViewModel.getCurrentChannel().getValue().equals(NotesListChannel.PRIVATE)) {
                            promptInfo = new BiometricPrompt.PromptInfo.Builder()
                                    .setTitle(getResources().getString(R.string.authenticate))
                                    .setDeviceCredentialAllowed(true)
                                    .setSubtitle(getResources().getString(R.string.authenticate_message))
                                    .build();

                            biometricPrompt.authenticate(promptInfo);
                        }
                        break;
                    case R.id.nav_deleted_notes:
                        if (!Objects.equals(mActivityViewModel.getCurrentChannel().getValue(), NotesListChannel.DELETED)) {
                            if (!mActivityViewModel.getMode().equals(ActivityToolbarMode.NORMAL))
                                startNormalMode();

                            mActivityViewModel.setCurrentChannel(NotesListChannel.DELETED);
                        }
                        break;
                    case R.id.nav_notes:
                        if (!Objects.equals(mActivityViewModel.getCurrentChannel().getValue(), NotesListChannel.NORMAL)) {
                            if (!mActivityViewModel.getMode().equals(ActivityToolbarMode.NORMAL))
                                startNormalMode();

                            mActivityViewModel.setCurrentChannel(NotesListChannel.NORMAL);
                        }
                        break;
                    case R.id.labels:
                        startActivity(new Intent(NotesListActivity.this, LabelsActivity.class));
                        break;
                }

                mNavigationView.setCheckedItem(menuItem.getItemId());
                mDrawer.closeDrawer(GravityCompat.START);

                return false;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == AddEditNoteActivity.ADDED_NOTE)
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    mNotesListRecyclerView.smoothScrollToPosition(0);
                }
            }, 200);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Objects.equals(mActivityViewModel.getMode(), ActivityToolbarMode.SEARCH)) return false;

        // Creates a context menu depending on the current mode
        if (!Objects.equals(mActivityViewModel.getMode(), ActivityToolbarMode.MULTI_SELECTION)) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.main_menu, menu);
        } else {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.toolbar_multi_selection_menu, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_all_notes:
                showDeleteAllDialog(mActivityViewModel.getCurrentChannel().getValue());

                break;
            case R.id.delete_selected:
                deleteSelectedNotes();
                startNormalMode();

                break;
            case R.id.change_color:
                showColorPicker();

                break;
            case R.id.change_label:
                if (mAllLabels != null) {
                    // Populating list
                    final String[] list = new String[mAllLabels.size()];
                    for (int i = 0; i < mAllLabels.size(); i++) {
                        list[i] = mAllLabels.get(i).getName();
                    }

                    final String[] chosenItems = new String[list.length];
                    final boolean[] checkItems = new boolean[list.length];

                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
                    mBuilder.setTitle("labels");
                    mBuilder.setMultiChoiceItems(list, checkItems, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
                            String label = list[position];

                            boolean contains = false;

                            for (String chosenItem : chosenItems) {
                                if (chosenItem != null && chosenItem.equals(label)) {
                                    contains = true;
                                    break;
                                }
                            }

                            if (isChecked) {
                                if (!contains) {
                                    chosenItems[position] = label;
                                }
                            } else if (contains) {
                                chosenItems[position] = null;
                            }
                        }
                    });

                    mBuilder.setPositiveButton("save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            for (int id : mNotesAdapter.getSelectedItemsPositions()) {
                                Note note = mNotesAdapter.getNoteAt(id);

                                for (int i = 0; i < chosenItems.length; i++) {
                                    if (chosenItems[i] != null) {
                                        Label l = mLabelsViewModel.getLabelByName(chosenItems[i]);

                                        if (l != null) {
                                            mNoteViewModel.addLabelToNote(note.getId(), l.getId());
                                        }
                                    }
                                }

                                mNoteViewModel.update(note);
                            }
                        }
                    });

                    mBuilder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Arrays.fill(chosenItems, null);
                            dialogInterface.dismiss();
                        }
                    });

                    mBuilder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            Arrays.fill(checkItems, false);
                        }
                    });

                    mDialog = mBuilder.create();
                    mDialog.show();
                }

                break;
            case R.id.view_layout:
                StaggeredGridLayoutManager gridLayoutManager = (StaggeredGridLayoutManager) mNotesListRecyclerView.getLayoutManager();
                if (Objects.requireNonNull(gridLayoutManager).getSpanCount() == 1) {
                    gridLayoutManager.setSpanCount(2);
                    mActivityViewModel.setRecyclerViewLayout(2);
                } else {
                    gridLayoutManager.setSpanCount(1);
                    mActivityViewModel.setRecyclerViewLayout(1);
                }

                mNotesAdapter.notifyItemRangeChanged(0, mNotesAdapter.getItemCount());
                mNotesListRecyclerView.setLayoutManager(gridLayoutManager);

            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        // If the drawer is open when back is pressed, we close it, else, we pass the event to the handler
        if (mDrawer.isDrawerOpen(GravityCompat.START))
            mDrawer.closeDrawer(GravityCompat.START);
        else if (Objects.equals(mActivityViewModel.getMode(), ActivityToolbarMode.MULTI_SELECTION))
            startNormalMode();
        else if (Objects.equals(mActivityViewModel.getMode(), ActivityToolbarMode.SEARCH))
            startNormalMode();
        else
            super.onBackPressed();
    }

    /**
     * Enters into a multi selection mode (sets a contextual action bar).
     */
    private void startMultiSelectionMode() {
        mActivityViewModel.setMode(ActivityToolbarMode.MULTI_SELECTION);
        Toolbar toolbar = mActivityViewModel.getToolbarsManager().renderToolbar(this, TOOLBAR_MULTI_SELECTION,
                mToolbarContainer, ToolbarsManager.ANIMATION_OUT);


        toolbar.removeViewAt(0);

        // Removes the hamburger icon preventing the drawer to open
        mDrawerToggle.setDrawerIndicatorEnabled(false);

        // Removes the main_toolbar title
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        // Creates the close button
        final ImageButton closeModeButton = new ImageButton(NotesListActivity.this);
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(ViewGroup.MarginLayoutParams.WRAP_CONTENT, ViewGroup.MarginLayoutParams.WRAP_CONTENT);
        lp.setMarginStart((int) getResources().getDimension(R.dimen.toolbar_close_btn_padding));
        lp.setMarginEnd((int) getResources().getDimension(R.dimen.toolbar_close_btn_padding));
        closeModeButton.setLayoutParams(lp);

        // A trick to allow animations of the toolbars between layouts
        int[] attrs = new int[]{R.attr.selectableItemBackgroundBorderless};
        TypedArray typedArray = obtainStyledAttributes(attrs);

        // Setting the color to the drawable icon
        Drawable unwrappedDrawable = AppCompatResources.getDrawable(NotesListActivity.this, R.drawable.ic_close);

        if (unwrappedDrawable != null) {
            Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
            DrawableCompat.setTint(wrappedDrawable, Color.BLACK);
            closeModeButton.setImageDrawable(wrappedDrawable);

            closeModeButton.setBackground(typedArray.getDrawable(0));
            typedArray.recycle();

            closeModeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startNormalMode();
                }
            });
        }

        try {
            toolbar.addView(closeModeButton, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        invalidateOptionsMenu();
    }

    private void startSearchMode() {
        mActivityViewModel.setMode(ActivityToolbarMode.SEARCH);
        mActivityViewModel.getToolbarsManager().renderToolbar(this, TOOLBAR_SEARCH,
                mToolbarContainer, ToolbarsManager.ANIMATION_OUT);

        mToolbarContainer.findViewById(R.id.search_field).requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm != null)
            imm.showSoftInput(mToolbarContainer.findViewById(R.id.search_field), InputMethodManager.SHOW_IMPLICIT);

        mToolbarContainer.findViewById(R.id.go_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNormalMode();
            }
        });

        ((EditText) mToolbarContainer.findViewById(R.id.search_field)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mNotesAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * Enters the normal mode. Changes main_toolbar accordingly.
     */
    private void startNormalMode() {
        mNotesAdapter.clearSelections();
        mActivityViewModel.setMode(ActivityToolbarMode.NORMAL);
        Toolbar t = mActivityViewModel.getToolbarsManager().renderToolbar(this, TOOLBAR_MAIN, mToolbarContainer, ToolbarsManager.ANIMATION_IN);
        mDrawerToggle.setDrawerIndicatorEnabled(true);

        t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSearchMode();
            }
        });

        // Displaying the drawer toggle icon after a delay to avoid collision with previous icons
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.setDrawerIndicatorEnabled(true);
            }
        }, 150);

        // Displays the title again
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        invalidateOptionsMenu();
    }

    @Override
    public void onItemClick(int notePosition) {
        if (mActivityViewModel.getMode().equals(ActivityToolbarMode.MULTI_SELECTION)) {
            toggleSelection(notePosition);
        } else {
            Intent startingIntent = new Intent(NotesListActivity.this, AddEditNoteActivity.class);
            Note note = mNotesAdapter.getNoteAt(notePosition);

            if (note != null) {
                startingIntent.putExtra(AddEditNoteActivity.EXTRA_NOTE, note);
                startActivityForResult(startingIntent, EDIT_NOTE_REQUEST);
            }
        }
    }

    @Override
    public void onItemLongClick(int notePosition) {
        if (!mActivityViewModel.getMode().equals(ActivityToolbarMode.MULTI_SELECTION))
            startMultiSelectionMode();

        toggleSelection(notePosition);
    }

    /**
     * Toggles the selection of the pressed note.
     *
     * @param position Position of the note in the adapter
     */
    private void toggleSelection(int position) {
        mNotesAdapter.toggleItemSelection(position);

        // If no items are selected then start normal mode
        if (mNotesAdapter.getSelectedItemsCount() == 0)
            startNormalMode();
        else if (mActivityViewModel.getMode().equals(ActivityToolbarMode.NORMAL))
            startMultiSelectionMode();

    }

    /**
     * Observes to the specified notes channel, while removing previous observers.
     *
     * @param notesListChannel The channel to observe to.
     */
    private void loadDataOnCurrentChannel(NotesListChannel notesListChannel) {
        removeObserversOfNotesList();

        switch (notesListChannel) {
            case NORMAL:
                mNoteViewModel.getAllExisting().observe(this, getDefaultObserver());
                mAddNoteFabButton.show();
                break;
            case DELETED:
                mNoteViewModel.getAllDeleted().observe(this, getDefaultObserver());
                mAddNoteFabButton.hide();
                break;
            case PRIVATE:
                mNoteViewModel.getAllPrivate().observe(this, getDefaultObserver());
                mAddNoteFabButton.hide();
                break;
        }
    }

    /**
     * @return The default observer who's result will feed the adapter
     */
    private Observer<List<Note>> getDefaultObserver() {
        return new Observer<List<Note>>() {
            @Override
            public void onChanged(List<Note> notes) {
                mNotesAdapter.submitList(notes);
            }
        };
    }

    /**
     * Remove all the notes observers
     */
    private void removeObserversOfNotesList() {
        mNoteViewModel.getAllExisting().removeObservers(this);
        mNoteViewModel.getAllPrivate().removeObservers(this);
        mNoteViewModel.getAllDeleted().removeObservers(this);
    }

    @Override
    public void onColorPick(String color) {
        // Updating the color of each selected note
        for (int id : mNotesAdapter.getSelectedItemsPositions()) {
            Note note = mNotesAdapter.getNoteAt(id);
            note.setColor(color);

            mNoteViewModel.update(note);
        }

        Fragment colorPickerFragment = getSupportFragmentManager().findFragmentByTag(COLOR_PICKER_FRAGMENT_TAG);

        // Closing the color picker fragment
        if (colorPickerFragment != null)
            getSupportFragmentManager().beginTransaction().remove(colorPickerFragment).commit();

        startNormalMode();
    }

    /**
     * Initializes Biometric authentication callbacks.
     */
    private void initBiometricAuthentication() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            Executor executor = getMainExecutor();
            biometricPrompt = new BiometricPrompt(this, executor, new BiometricPrompt.AuthenticationCallback() {
                @Override
                public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                    Toast.makeText(NotesListActivity.this, getString(R.string.authentication_error), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onAuthenticationSucceeded(
                        @NonNull BiometricPrompt.AuthenticationResult result) {
                    if (!mActivityViewModel.getMode().equals(ActivityToolbarMode.NORMAL))
                        startNormalMode();

                    mActivityViewModel.setCurrentChannel(NotesListChannel.PRIVATE);
                }

                @Override
                public void onAuthenticationFailed() {
                    Toast.makeText(NotesListActivity.this, getString(R.string.authentication_error), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Displays the confirmation dialog of deleting all the notes to the user. On confirmation,
     * all notes are deleted (moved to trash tab).
     */
    private void showDeleteAllDialog(final NotesListChannel channel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.delete_all_notes_title)).setMessage(getString(R.string.delete_all_notes_message));

        builder.setPositiveButton(getString(R.string.delete_all_notes_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                final List<Note> allNotes = mNoteViewModel.getNotesFromChannel(channel);
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                for (Note noteToDelete : allNotes) {
                    Alarm alarmToDelete = mAlarmsViewModel.getByNote(noteToDelete.getId());

                    if (alarmToDelete != null) {
                        Intent intent = new Intent(NotesListActivity.this, AlertReceiver.class);
                        intent.putExtra("extra_id", alarmToDelete.getId());
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), alarmToDelete.getId(), intent, 0);

                        Objects.requireNonNull(alarmManager).cancel(pendingIntent);

                        mAlarmsViewModel.delete(alarmToDelete);
                    }

                    if (!channel.equals(NotesListChannel.DELETED))
                        mNoteViewModel.moveToTrash(noteToDelete);
                    else
                        mNoteViewModel.delete(noteToDelete);
                }

                if (!channel.equals(NotesListChannel.DELETED))
                    Snackbar.make(findViewById(R.id.myCoordinator), getString(R.string.undo_selected_message), 4000)
                            .setAction(getString(R.string.undo), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    for (Note note : allNotes) {
                                        mNoteViewModel.restoreFromTrash(note);
                                    }
                                }
                            }).show();
            }
        });

        builder.setNegativeButton(getString(R.string.delete_all_notes_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        builder.create().show();
    }

    /**
     * Deletes notes that were selected in multi selection mode. A snack bar will trigger to undo the operation.
     */
    private void deleteSelectedNotes() {
        final List<Note> selectedNotes = new ArrayList<>();

        if (Objects.equals(mActivityViewModel.getCurrentChannel().getValue(), NotesListChannel.DELETED)) {
            for (int id : mNotesAdapter.getSelectedItemsPositions()) {
                Note note = mNotesAdapter.getNoteAt(id);
                selectedNotes.add(note);
                mNoteViewModel.delete(note);
            }
        } else {
            for (int id : mNotesAdapter.getSelectedItemsPositions()) {
                Note note = mNotesAdapter.getNoteAt(id);
                selectedNotes.add(note);
                mNoteViewModel.moveToTrash(note);
            }
        }

        if (!mActivityViewModel.getCurrentChannel().getValue().equals(NotesListChannel.DELETED))
            Snackbar.make(findViewById(R.id.myCoordinator), getString(R.string.undo_selected_message), 4000)
                    .setAction(getString(R.string.undo), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            for (Note note : selectedNotes) {
                                mNoteViewModel.restoreFromTrash(note);
                            }
                        }
                    }).show();
    }

    /**
     * Shows the color picker fragment
     */
    private void showColorPicker() {
        List<String> colors = new ArrayList<>();

        try {
            colors = Configuration.getColors(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ColorPickerFragment colorPickerFragment = ColorPickerFragment.newInstance(colors);
        colorPickerFragment.show(getSupportFragmentManager(), COLOR_PICKER_FRAGMENT_TAG);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (direction == ItemTouchHelper.START || direction == ItemTouchHelper.END) {
            if (!mActivityViewModel.getMode().equals(ActivityToolbarMode.NORMAL))
                startNormalMode();

            // Swipe to delete
            final Note noteToDelete = mNotesAdapter.getNoteAt(viewHolder.getAdapterPosition());

            if (direction == ItemTouchHelper.START && Objects.equals(mActivityViewModel.getCurrentChannel().getValue(), NotesListChannel.DELETED))
                mNoteViewModel.restoreFromTrash(noteToDelete);
            else {
                if (direction == ItemTouchHelper.START)
                    mNotesAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                else if (Objects.equals(mActivityViewModel.getCurrentChannel().getValue(), NotesListChannel.DELETED))
                    mNoteViewModel.delete(noteToDelete);
                else {
                    mNoteViewModel.moveToTrash(noteToDelete);


                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                    Alarm alarmToDelete = mAlarmsViewModel.getByNote(noteToDelete.getId());
                    if (alarmToDelete != null) {
                        Intent intent = new Intent(getApplicationContext(), AlertReceiver.class);
                        intent.putExtra("extra_id", alarmToDelete.getId());
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), alarmToDelete.getId(), intent, 0);

                        Objects.requireNonNull(alarmManager).cancel(pendingIntent);
                        mAlarmsViewModel.delete(alarmToDelete);
                    }

                    if (!mActivityViewModel.getCurrentChannel().getValue().equals(NotesListChannel.DELETED))
                        Snackbar.make(findViewById(R.id.myCoordinator), R.string.undo_note_deletion, 4000)
                                .setAction(R.string.undo, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mNoteViewModel.restoreFromTrash(noteToDelete);
                                    }
                                }).show();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mDialog != null)
            mDialog.dismiss();
    }

    private void initToolbarsManager() {
        ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

        Toolbar mainToolbar = (Toolbar) getLayoutInflater().inflate(R.layout.main_toolbar, rootView, false);
        Toolbar searchToolbar = (Toolbar) getLayoutInflater().inflate(R.layout.search_toolbar, rootView, false);
        Toolbar multiSelectionToolbar = (Toolbar) getLayoutInflater().inflate(R.layout.multi_selection_toolbar, rootView, false);

        ToolbarsManager toolbarsManager = mActivityViewModel.getToolbarsManager();
        toolbarsManager.addToolbar(TOOLBAR_MAIN, mainToolbar);
        toolbarsManager.addToolbar(TOOLBAR_SEARCH, searchToolbar);
        toolbarsManager.addToolbar(TOOLBAR_MULTI_SELECTION, multiSelectionToolbar);

        final LinearLayout.LayoutParams layoutParams;

        if (mToolbarContainer.getLayoutParams() == null)
            mToolbarContainer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        layoutParams = (LinearLayout.LayoutParams) mToolbarContainer.getLayoutParams();


        // Set in animations, margin animation
        int marginToReach = getResources().getDimensionPixelSize(R.dimen.toolbar_wrapper_margin);
        ValueAnimator inMarginAnimator = ValueAnimator.ofInt(0, marginToReach);
        inMarginAnimator.setDuration(200);
        inMarginAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                layoutParams.setMargins((Integer) animation.getAnimatedValue(), (Integer) animation.getAnimatedValue(), (Integer) animation.getAnimatedValue(), (Integer) animation.getAnimatedValue());
                mToolbarContainer.setLayoutParams(layoutParams);
            }
        });

        // Border radius animation
        int radiusToReach = getResources().getDimensionPixelSize(R.dimen.toolbar_wrapper_corner_radius);
        ValueAnimator inBorderRadiusAnimator = ValueAnimator.ofInt(0, radiusToReach);
        inBorderRadiusAnimator.setDuration(200);
        inBorderRadiusAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mToolbarContainer.setRadius((int) animation.getAnimatedValue());
            }
        });

        // Set out animations, margin animation
        int marginToReach2 = 0;
        ValueAnimator outMarginAnimator = ValueAnimator.ofInt(layoutParams.leftMargin, marginToReach2);
        outMarginAnimator.setDuration(200);
        outMarginAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                layoutParams.setMargins((Integer) animation.getAnimatedValue(), (Integer) animation.getAnimatedValue(), (Integer) animation.getAnimatedValue(), (Integer) animation.getAnimatedValue());
                mToolbarContainer.setLayoutParams(layoutParams);
            }
        });

        int radiusToReach2 = 0;
        ValueAnimator outBorderRadiusAnimator = ValueAnimator.ofInt((int) mToolbarContainer.getRadius(), radiusToReach2);
        outBorderRadiusAnimator.setDuration(200);
        outBorderRadiusAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mToolbarContainer.setRadius((int) animation.getAnimatedValue());
            }
        });

        AnimatorSet inAnimatorSet = new AnimatorSet();
        inAnimatorSet.playTogether(inMarginAnimator, inBorderRadiusAnimator);

        AnimatorSet outAnimatorSet = new AnimatorSet();
        outAnimatorSet.playTogether(outMarginAnimator, outBorderRadiusAnimator);

        toolbarsManager.setmInAnimation(inAnimatorSet);
        toolbarsManager.setmOutAnimation(outAnimatorSet);
    }
    // endregion
}