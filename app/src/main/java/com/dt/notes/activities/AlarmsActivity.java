package com.dt.notes.activities;

import android.os.Bundle;

import com.dt.notes.adapters.AlarmAdapter;
import com.dt.notes.entities.Alarm;
import com.dt.notes.viewmodels.AlarmsViewModel;
import com.dt.notes.viewmodels.NoteViewModel;
import com.dt.notes.viewmodels.SelectedItemsViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dt.notes.R;

import java.util.List;

public class AlarmsActivity extends AppCompatActivity {
    private RecyclerView mAlaramsRecyclerView;
    private AlarmAdapter mAlarmAdapter;
    private AlarmsViewModel mAlarmViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarms);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAlaramsRecyclerView = findViewById(R.id.alarm_rv);
        SelectedItemsViewModel selectedItemsViewModel = ViewModelProviders.of(this).get(SelectedItemsViewModel.class);
        NoteViewModel noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);

        mAlarmAdapter = new AlarmAdapter(selectedItemsViewModel, noteViewModel);
        mAlaramsRecyclerView.setAdapter(mAlarmAdapter);
        mAlaramsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAlarmViewModel = ViewModelProviders.of(this).get(AlarmsViewModel.class);

        mAlarmViewModel.getAll().observe(this, new Observer<List<Alarm>>() {
            @Override
            public void onChanged(List<Alarm> alarms) {
                mAlarmAdapter.submitList(alarms);
            }
        });

        Toolbar t = (Toolbar) getLayoutInflater().inflate(R.layout.main_toolbar, null);
        setSupportActionBar(t);
        getSupportActionBar().setTitle("ffgfg");
    }

}
