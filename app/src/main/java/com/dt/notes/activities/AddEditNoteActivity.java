package com.dt.notes.activities;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.dt.notes.R;
import com.dt.notes.config.Configuration;
import com.dt.notes.entities.Alarm;
import com.dt.notes.entities.Label;
import com.dt.notes.entities.Note;
import com.dt.notes.entities.NoteLabels;
import com.dt.notes.fragments.ColorPickerFragment;
import com.dt.notes.fragments.DateTimePickerDialogFragment;
import com.dt.notes.fragments.MultiChoicePickerFragment;
import com.dt.notes.receivers.AlertReceiver;
import com.dt.notes.viewmodels.AlarmsViewModel;
import com.dt.notes.viewmodels.LabelsViewModel;
import com.dt.notes.viewmodels.NoteViewModel;
import com.google.android.material.chip.Chip;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

/**
 * <h1>AddEditNoteActivity</h1>
 *
 * <p>
 * Responsible for creating a new note or editing an existing one.
 * </p>
 */
public class AddEditNoteActivity extends AppCompatActivity implements ColorPickerFragment.ColorPickListener, DateTimePickerDialogFragment.OnDateTimePickListener {
    // Constants
    public static final String EXTRA_NOTE = "com.dt.notes.activities.EXTRA_NOTE";
    public static final int ADDED_NOTE = -10;
    public static final int EDITED_NOTE = -11;
    private static final String COLOR_PICKER_FRAGMENT_TAG = "AddEditNoteActivity.ColorPickerFragment";

    // Properties
    private EditText mEditTextTitle;
    private EditText mEditTextDescription;
    private Calendar mCalendar;
    private DateTimePickerDialogFragment dateTimePickerDialogFragment;
    private Note mEditedNote;
    private NoteViewModel mNoteViewModel;
    private AlarmsViewModel mAlarmsViewModel;
    private ToggleButton mPrivateToggle;
    private boolean mAlarmSet;
    private ImageButton mShareButton;
    private View mRootView;
    private Button mPickLabelsBtn;
    Button timePickerButton;
    List<Label> mCurrentLabels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        initViews();
        initEditedNote();

        // Init labels
        if (getIntent().hasExtra(EXTRA_NOTE)) {
            mNoteViewModel.getNoteLabels(mEditedNote.getId()).observe(this, new Observer<NoteLabels>() {
                @Override
                public void onChanged(NoteLabels noteLabels) {
                    mCurrentLabels = noteLabels.labels;

                    LinearLayout linearLayout = findViewById(R.id.labels_container);
                    linearLayout.removeAllViews();

                    for (Label l : noteLabels.labels) {
                        Chip chip = new Chip(AddEditNoteActivity.this);
                        chip.setText(l.getName());
                        ViewGroup.MarginLayoutParams marginLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        marginLayoutParams.setMarginEnd(10);
                        chip.setLayoutParams(marginLayoutParams);
                        linearLayout.addView(chip);
                    }
                }
            });
        }
    }

    /**
     * Initiating view objects.
     */
    private void initViews() {
        Button colorPickerButton = findViewById(R.id.colorBtn);
        timePickerButton = findViewById(R.id.timePicker);
        Toolbar toolbar = findViewById(R.id.toolbarEditNote);
        mPrivateToggle = findViewById(R.id.secureToggle);

        mCalendar = Calendar.getInstance();
        mEditTextTitle = findViewById(R.id.et_title);
        mEditTextDescription = findViewById(R.id.et_description);
        mShareButton = findViewById(R.id.share_btn);
        mPickLabelsBtn = findViewById(R.id.pick_labels);

        final Intent startingIntent = getIntent();

        if (startingIntent.hasExtra(EXTRA_NOTE)) {
            mPickLabelsBtn.setVisibility(View.VISIBLE);

            mShareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Note note = (Note) startingIntent.getSerializableExtra(EXTRA_NOTE);
                    String formatToShare = String.format("%s\n%s\n%s", note.getTitle(), note.getDescription(), note.getCreateDate());

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, formatToShare);
                    sendIntent.setType("text/plain");

                    Intent shareIntent = Intent.createChooser(sendIntent, null);
                    startActivity(shareIntent);
                }
            });

            mShareButton.setVisibility(View.VISIBLE);
        }

        mNoteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        mAlarmsViewModel = ViewModelProviders.of(this).get(AlarmsViewModel.class);

        toolbar.setNavigationIcon(R.drawable.ic_close);
        setSupportActionBar(toolbar);

        List<String> colors = new ArrayList<>();

        try {
            colors = Configuration.getColors(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final ColorPickerFragment colorPickerFragment = ColorPickerFragment.newInstance(colors);

        colorPickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorPickerFragment.show(getSupportFragmentManager(), COLOR_PICKER_FRAGMENT_TAG);
            }
        });

        timePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTimePickerDialogFragment = new DateTimePickerDialogFragment();
                dateTimePickerDialogFragment.show(getSupportFragmentManager(), "bla");

            }
        });

        final LabelsViewModel labelsViewModel = ViewModelProviders.of(this).get(LabelsViewModel.class);
        final List<Label> mLabels = new ArrayList<>();

        labelsViewModel.getAllLabels().observe(this, new Observer<List<Label>>() {
            @Override
            public void onChanged(List<Label> labels) {
                mLabels.clear();
                mLabels.addAll(labels);
            }
        });

        mPickLabelsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] list = new String[mLabels.size()];
                for (int i = 0; i < mLabels.size(); i++) {
                    list[i] = mLabels.get(i).getName();
                }

                final String[] chosenItems = new String[list.length];

                //SimpleCheckBoxPickerFragment<Label> checkBoxPickerFragment = new SimpleCheckBoxPickerFragment<>(mLabels);
                //checkBoxPickerFragment.show(getSupportFragmentManager(), "gg");

                boolean[] checkItems = new boolean[list.length];

                if (mCurrentLabels != null) {
                    for (Label l : mCurrentLabels) {
                        for (int i = 0; i < list.length; i++) {
                            if (list[i].equals(l.getName())) {
                                checkItems[i] = true;
                                chosenItems[i] = l.getName();
                                break;
                            }
                        }
                    }
                }

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(AddEditNoteActivity.this);
                mBuilder.setTitle("labels");
                mBuilder.setMultiChoiceItems(list, checkItems, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
                        String label = list[position];

                        boolean contains = false;

                        for (int i = 0; i < chosenItems.length; i++) {
                            if (chosenItems[i] != null && chosenItems[i].equals(label)) {
                                contains = true;
                            }
                        }

                        if (isChecked) {
                            if (!contains) {
                                chosenItems[position] = label;
                            }
                        } else if (contains) {
                            chosenItems[position] = null;
                        }
                    }
                });

                mBuilder.setPositiveButton("save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        mNoteViewModel.deleteLabelsOfNote(mEditedNote.getId());

                        for (int i = 0; i < chosenItems.length; i++) {
                            for (int j = 0; j < mLabels.size(); j++) {
                                Label l = mLabels.get(j);

                                if (chosenItems[i] != null && chosenItems[i].equals(l.getName())) {
                                    mNoteViewModel.addLabelToNote(mEditedNote.getId(), l.getId());
                                    break;
                                }
                            }
                        }

                        mNoteViewModel.getNoteLabels(mEditedNote.getId());
                    }
                });

                mBuilder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Arrays.fill(chosenItems, null);
                        dialogInterface.dismiss();
                    }
                });

                mBuilder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        mNoteViewModel.deleteLabelsOfNote(mEditedNote.getId());
                    }
                });

                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });
    }

    private void initEditedNote() {
        Intent startingIntent = getIntent();

        // If we edit an existing note
        if (startingIntent.hasExtra(EXTRA_NOTE)) {
            setTitle(getResources().getString(R.string.edit_note_title));
            mEditedNote = (Note) startingIntent.getSerializableExtra(EXTRA_NOTE);

            mEditTextTitle.setText(mEditedNote.getTitle());
            mEditTextDescription.setText(mEditedNote.getDescription());

            Alarm alarm = mAlarmsViewModel.getByNote(mEditedNote.getId());

            if (alarm != null) {
                mAlarmSet = false;
                Date date = new Date(alarm.getAlarmTime());
                DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
                timePickerButton.setText(dateFormat.format(date));
            }

            mRootView = findViewById(R.id.edit_layout).getRootView();

            if (!mEditedNote.getColor().equals(""))
                mRootView.setBackgroundColor(Color.parseColor(mEditedNote.getColor()));
        } else {
            mEditedNote = new Note("", "", "", false, false);
            setTitle(getResources().getString(R.string.add_note_title));
        }
    }

    private void saveNote() {
        String title = mEditTextTitle.getText().toString();
        String description = mEditTextDescription.getText().toString();

        if (title.trim().isEmpty() || description.trim().isEmpty()) {
            Toast.makeText(this, "Please inset a title and description", Toast.LENGTH_SHORT).show();

            return;
        }

        mEditedNote.setIsPrivate(mPrivateToggle.isChecked());

        // Save Existing note
        if (getIntent().getSerializableExtra(EXTRA_NOTE) != null) {
            Note note = (Note) getIntent().getSerializableExtra(EXTRA_NOTE);

            if (note != null)
                mEditedNote.setId(note.getId());

            mEditedNote.setTitle(title);
            mEditedNote.setDescription(description);

            mNoteViewModel.update(mEditedNote);

            if (mAlarmsViewModel.getByNote(mEditedNote.getId()) != null && mAlarmSet) {
                Alarm atd = mAlarmsViewModel.getByNote(mEditedNote.getId());
                mAlarmsViewModel.delete(atd);
                cancelAlarm(atd.getId());

                Alarm a = new Alarm(mEditedNote.getId(), mCalendar.getTimeInMillis());

                long generatedId = mAlarmsViewModel.insert(a);

                startAlarm(mCalendar, generatedId);
            } else if (mAlarmsViewModel.getByNote(mEditedNote.getId()) == null && mAlarmSet) {
                Alarm a = new Alarm(mEditedNote.getId(), mCalendar.getTimeInMillis());
                long generatedId = mAlarmsViewModel.insert(a);

                startAlarm(mCalendar, generatedId);
            }

            setResult(EDITED_NOTE);
        }
        // Add note
        else {
            mEditedNote.setTitle(title);
            mEditedNote.setDescription(description);
            long id = mNoteViewModel.insert(mEditedNote);

            if (mAlarmSet) {
                AlarmsViewModel alarmsViewModel = ViewModelProviders.of(this).get(AlarmsViewModel.class);
                Alarm a = new Alarm((int) id, mCalendar.getTimeInMillis());
                long generatedId = alarmsViewModel.insert(a);
                startAlarm(mCalendar, generatedId);
            }

            setResult(ADDED_NOTE);
        }

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_note_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.save_note) {
            saveNote();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onColorPick(String color) {
        mEditedNote.setColor(color);

        // Find the root view
        final View root = getWindow().getDecorView();

        int fromColor = ((ColorDrawable) root.getBackground()).getColor();
        int toColor = Color.parseColor(color);

        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), fromColor, toColor);
        colorAnimation.setDuration(250);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                root.setBackgroundColor((int) animator.getAnimatedValue());
            }

        });

        colorAnimation.start();
    }

    private void startAlarm(Calendar c, long alarmId) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        intent.putExtra("alarmId", (int) alarmId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), (int) alarmId, intent, 0);

        if (c.before(Calendar.getInstance()))
            c.add(Calendar.DATE, 1);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
    }

    private void cancelAlarm(int alarmId) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AlertReceiver.class);
        intent.putExtra("extra_id", alarmId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), alarmId, intent, 0);

        alarmManager.cancel(pendingIntent);
    }

    @Override
    public void onDateTimePickFinished(Calendar calendar) {
        onDateTimePickerDismiss();

        mCalendar = calendar;
        mAlarmSet = true;
    }

    @Override
    public void onDateTimePickerDismiss() {
        dateTimePickerDialogFragment.dismiss();
    }
}
